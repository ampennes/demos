int dir_pin=0;
int step_pin=1;
int DAC_pin=2;
int M1_pin=3;
int M0_pin=4;
int S0_pin=7;
int S1_pin=6;
int S2_pin=5;
int speed_pin=8;
int step_time=0;
volatile int S0=0;
volatile int S1=0;
volatile int S2=0;
volatile int mode=0;
void setup() {
  delay(2000);
  // put your setup code here, to run once:
Serial.begin(0);
delay(10);
//while(!Serial);
delay(2000);
analogReadResolution(8); // can totally change this, just convenient if I want~4 steps per second at the slowest anyway
Serial.println("setup");
pinMode(dir_pin,OUTPUT);
pinMode(step_pin,OUTPUT);
pinMode(DAC_pin,OUTPUT);
pinMode(M1_pin,OUTPUT);
pinMode(M0_pin,OUTPUT);
pinMode(S0_pin,INPUT);
pinMode(S1_pin,INPUT);
pinMode(S2_pin,INPUT);
pinMode(speed_pin,INPUT);
digitalWrite(dir_pin, HIGH);
//analogWrite(DAC_pin, 220);
digitalWrite(M0_pin, HIGH);
digitalWrite(M1_pin, HIGH);// both low full steps
digitalWrite(step_pin,LOW);

S0=digitalRead(S0_pin);
S1=digitalRead(S1_pin);
S2=digitalRead(S2_pin);// take the states on startup.  Only read again on pin change interrupt

mode=S0+(S1<<1)+(S2<<2);// bit shifting 1 and 2 should give 8 unique results for mode that we'll use in a switch statement to determine the stepping mode
Serial.print("Mode=  ");
Serial.println(mode);
attachInterrupt(digitalPinToInterrupt(S0_pin), Interrupt, CHANGE);
attachInterrupt(digitalPinToInterrupt(S1_pin), Interrupt, CHANGE);
attachInterrupt(digitalPinToInterrupt(S2_pin), Interrupt, CHANGE);// setup pin change interrupts on all 3 step input pins all pointing to the same function
}

void loop() {
// alright pin states are handled via the ISR. all that's left is to do step timing
// maybe add in current after that
// wonder what time overhead is like here... maybe only read form the ADC every couple steps/miliseconds?
int inc= analogRead(speed_pin); // now how do I want to scale this? 1ms to 250ms? Might as well go 8 bit adc then?
inc=inc+2; // so it can't be 0
digitalWrite(step_pin, HIGH);
delay(inc/2); // meh not the fastest but probably sufficient
digitalWrite(step_pin, LOW);
delay(inc/2);



}
void Interrupt(){// dumb way, read all 3 pins, set all modes.  Faster way, instill state, different interrupts for each pin, update only what is needed.  Probably not needed.  Might do it anyway to see execution time
S0=digitalRead(S0_pin);
S1=digitalRead(S1_pin);
S2=digitalRead(S2_pin);
mode=S0+(S1<<1)+(S2<<2);
switch(mode){
case 0:
Step_Full();
break;
case 1:
Step_Half();
break;
case 2:
Step1_4();
break;
case 3:
Step1_8();
break;
case 4:
Step1_16();
break;
case 5:
Step1_32();
break;
case 6:
Step1_128();
break;
case 7:
Step1_256();
break;
}
}
void Step_Full(){
  pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M0_pin,LOW);
  digitalWrite(M1_pin,LOW);
}

void Step_Half(){
  pinMode(M0_pin, INPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M1_pin,LOW);
}

void Step1_4(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M0_pin,LOW);
  digitalWrite(M1_pin,HIGH);
}
void Step1_8(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M0_pin,HIGH);
  digitalWrite(M1_pin,HIGH);
}
void Step1_16(){
    pinMode(M0_pin, INPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M1_pin,HIGH);
}
void Step1_32(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, INPUT);
  digitalWrite(M0_pin,LOW);
  digitalWrite(M1_pin,LOW);
}

void Step1_128(){
    pinMode(M0_pin, INPUT);
  pinMode(M1_pin, INPUT);
  
}
void Step1_256(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, INPUT);
  digitalWrite(M0_pin,LOW);
  
}
