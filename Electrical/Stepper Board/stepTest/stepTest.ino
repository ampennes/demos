int dir_pin=0;
int step_pin=1;
int DAC_pin=2;
int M1_pin=3;
int M0_pin=4;
int red_pin=5;
int S0_pin=7;
int S1_pin=6;
int S2_pin=10;
int speed_pin=8;
int current_pin=9;

void setup() {
  delay(2000);
  // put your setup code here, to run once:
Serial.begin(0);
delay(10);
while(!Serial);
delay(2000);
Serial.println("setup");
pinMode(dir_pin,OUTPUT);
pinMode(step_pin,OUTPUT);
pinMode(DAC_pin,OUTPUT);
pinMode(M1_pin,OUTPUT);
pinMode(M0_pin,OUTPUT);
pinMode(S0_pin,INPUT);
pinMode(S1_pin,INPUT);
pinMode(S2_pin,INPUT);
pinMode(speed_pin,INPUT);
pinMode(current_pin,INPUT);
digitalWrite(dir_pin, HIGH);
//analogWrite(DAC_pin, 220);
digitalWrite(M0_pin, HIGH);
digitalWrite(M1_pin, HIGH);// both low full steps
digitalWrite(step_pin,LOW);


}

void loop() {
  // put your main code here, to run repeatedly:
//Serial.println("loop");
//delay(1000);
Serial.println("loop");
pinMode(M0_pin, OUTPUT);
pinMode(M1_pin,OUTPUT);
digitalWrite(M0_pin,LOW);
digitalWrite(M1_pin,LOW);// both high 1/8 stepping
digitalWrite(dir_pin, HIGH);



}
void Step_Full(){
  pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M0_pin,LOW);
  digitalWrite(M1_pin,LOW);
}

void Step_Half(){
  pinMode(M0_pin, INPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M1_pin,LOW);
}

void Step1_4(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M0_pin,LOW);
  digitalWrite(M1_pin,HIGH);
}
void Step1_8(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M0_pin,HIGH);
  digitalWrite(M1_pin,HIGH);
}
void Step1_16(){
    pinMode(M0_pin, INPUT);
  pinMode(M1_pin, OUTPUT);
  digitalWrite(M1_pin,HIGH);
}
void Step1_32(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, INPUT);
  digitalWrite(M0_pin,LOW);
  digitalWrite(M1_pin,LOW);
}

void Step1_128(){
    pinMode(M0_pin, INPUT);
  pinMode(M1_pin, INPUT);
  
}
void Step1_256(){
    pinMode(M0_pin, OUTPUT);
  pinMode(M1_pin, INPUT);
  digitalWrite(M0_pin,LOW);
  
}
