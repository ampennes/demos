Thoughts on the stepper board:
- uses the A4950 for now could go drv8846 for built in microstepping
- how does microstepping work on this guy?
- also good link re: the downside of microstepping  https://www.faulhaber.com/en/support/technical-support/motors/tutorials/stepper-motor-tutorial-microstepping-myths-and-realities/
- another good link http://www.lamja.com/?p=140
- Talks about why we drive them at higher voltages and use chopping.  Due to high inductance of the windings reducing max speed.
- how do you get phased PWMs?  It seems like a common tactic is to just use lookup tables with pointers to account for the phase delta.  Not really using the PWM module to generate the low freq PWM itself but rather a high freq PWM and varying the duty cycle based on the loaction.  D21 only has 1 DAC so can't use that.
- I think this should have 2 boards.  One board using dual h bridge and controlling stepps and an attempt at microstepping.  Second board with more complicated controller for dedicated stepper control with built in micro stepping.  Shows the trade off between fewer pins, simplier hardware, and much more time writing software, and dedicated hardware that should be easier to work with but is more expensive and takes more board realestate/pins.  Everything is a spectrum.  Where do you want to be?
- It would be cool if we could throw some measurementation on this.  shaft rpm would be fine (though technically a good approximation can be done just from the code) but measuring torque to see how it decays with microstepping could be really nice.  Could be as simple as a weighted wheel that spins properly with normal steps but gets jumpy with microsteps?  I it might be hard to notice unless the diameter is pretty high.
- 
