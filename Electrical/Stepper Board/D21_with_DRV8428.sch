<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.6.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="1" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="1" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="1" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="1" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="1" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="1" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="1" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="1" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="1" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="1" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="1" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="1" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="1" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="1" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ATSAMD21E18A-AUT">
<packages>
<package name="QFP80P900X900X120-32N">
<text x="-5.225" y="-5.33" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-5.225" y="5.43" size="1.27" layer="25">&gt;NAME</text>
<circle x="-5.98" y="2.8" radius="0.1" width="0.2" layer="21"/>
<circle x="-5.98" y="2.8" radius="0.1" width="0.2" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="3.5" x2="-3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="3.395" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.395" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.395" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.395" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="-3.395" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="3.395" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="-3.395" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="3.395" width="0.127" layer="21"/>
<wire x1="-5.23" y1="-5.23" x2="5.23" y2="-5.23" width="0.05" layer="39"/>
<wire x1="-5.23" y1="5.23" x2="5.23" y2="5.23" width="0.05" layer="39"/>
<wire x1="-5.23" y1="-5.23" x2="-5.23" y2="5.23" width="0.05" layer="39"/>
<wire x1="5.23" y1="-5.23" x2="5.23" y2="5.23" width="0.05" layer="39"/>
<smd name="9" x="-2.8" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="-2" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="-1.2" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="-0.4" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="13" x="0.4" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="14" x="1.2" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="15" x="2" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="16" x="2.8" y="-4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="25" x="2.8" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="26" x="2" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="27" x="1.2" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="28" x="0.4" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="29" x="-0.4" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="30" x="-1.2" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="31" x="-2" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="32" x="-2.8" y="4.18" dx="1.6002" dy="0.39243125" layer="1" roundness="25" rot="R90"/>
<smd name="1" x="-4.18" y="2.8" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="2" x="-4.18" y="2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="3" x="-4.18" y="1.2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="4" x="-4.18" y="0.4" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="5" x="-4.18" y="-0.4" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="6" x="-4.18" y="-1.2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="7" x="-4.18" y="-2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="8" x="-4.18" y="-2.8" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="17" x="4.18" y="-2.8" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="18" x="4.18" y="-2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="19" x="4.18" y="-1.2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="20" x="4.18" y="-0.4" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="21" x="4.18" y="0.4" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="22" x="4.18" y="1.2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="23" x="4.18" y="2" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
<smd name="24" x="4.18" y="2.8" dx="1.6002" dy="0.39243125" layer="1" roundness="25"/>
</package>
</packages>
<symbols>
<symbol name="ATSAMD21E18A-AUT">
<wire x1="-12.7" y1="25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<text x="-12.7" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PA00" x="-17.78" y="10.16" length="middle"/>
<pin name="PA01" x="-17.78" y="7.62" length="middle"/>
<pin name="PA02" x="-17.78" y="5.08" length="middle"/>
<pin name="PA03" x="-17.78" y="2.54" length="middle"/>
<pin name="PA04" x="-17.78" y="0" length="middle"/>
<pin name="PA05" x="-17.78" y="-2.54" length="middle"/>
<pin name="PA06" x="-17.78" y="-5.08" length="middle"/>
<pin name="PA07" x="-17.78" y="-7.62" length="middle"/>
<pin name="PA08" x="-17.78" y="-10.16" length="middle"/>
<pin name="PA09" x="-17.78" y="-12.7" length="middle"/>
<pin name="PA10" x="-17.78" y="-15.24" length="middle"/>
<pin name="PA11" x="-17.78" y="-17.78" length="middle"/>
<pin name="PA14" x="-17.78" y="-20.32" length="middle"/>
<pin name="PA15" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="PA16" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="PA17" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="PA18" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="PA19" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="PA22" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="PA23" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PA24" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PA25" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="PA27" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="PA28" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="PA30" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="PA31" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="!RESET" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="VDDANA" x="17.78" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDIN" x="17.78" y="20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDCORE" x="17.78" y="22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-25.4" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD21E18A-AUT" prefix="U">
<description>ARM Cortex-M0+ SAM D21E Microcontroller IC 32-Bit 48MHz 256KB (256K x 8) FLASH 32-TQFP (7x7)   &lt;a href="https://pricing.snapeda.com/parts/ATSAMD21E18A-AU/Microchip%20Technology/view-part?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATSAMD21E18A-AUT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP80P900X900X120-32N">
<connects>
<connect gate="G$1" pin="!RESET" pad="26"/>
<connect gate="G$1" pin="GND" pad="10 28"/>
<connect gate="G$1" pin="PA00" pad="1"/>
<connect gate="G$1" pin="PA01" pad="2"/>
<connect gate="G$1" pin="PA02" pad="3"/>
<connect gate="G$1" pin="PA03" pad="4"/>
<connect gate="G$1" pin="PA04" pad="5"/>
<connect gate="G$1" pin="PA05" pad="6"/>
<connect gate="G$1" pin="PA06" pad="7"/>
<connect gate="G$1" pin="PA07" pad="8"/>
<connect gate="G$1" pin="PA08" pad="11"/>
<connect gate="G$1" pin="PA09" pad="12"/>
<connect gate="G$1" pin="PA10" pad="13"/>
<connect gate="G$1" pin="PA11" pad="14"/>
<connect gate="G$1" pin="PA14" pad="15"/>
<connect gate="G$1" pin="PA15" pad="16"/>
<connect gate="G$1" pin="PA16" pad="17"/>
<connect gate="G$1" pin="PA17" pad="18"/>
<connect gate="G$1" pin="PA18" pad="19"/>
<connect gate="G$1" pin="PA19" pad="20"/>
<connect gate="G$1" pin="PA22" pad="21"/>
<connect gate="G$1" pin="PA23" pad="22"/>
<connect gate="G$1" pin="PA24" pad="23"/>
<connect gate="G$1" pin="PA25" pad="24"/>
<connect gate="G$1" pin="PA27" pad="25"/>
<connect gate="G$1" pin="PA28" pad="27"/>
<connect gate="G$1" pin="PA30" pad="31"/>
<connect gate="G$1" pin="PA31" pad="32"/>
<connect gate="G$1" pin="VDDANA" pad="9"/>
<connect gate="G$1" pin="VDDCORE" pad="29"/>
<connect gate="G$1" pin="VDDIN" pad="30"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Good"/>
<attribute name="DESCRIPTION" value=" IC MCU 32BIT 256KB FLASH 32TQFP "/>
<attribute name="MF" value="Microchip Technology"/>
<attribute name="MP" value="ATSAMD21E18A-AU"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/ATSAMD21E18A-AU/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DRV8428">
<packages>
<package name="DRV8428PWP">
<smd name="P$1" x="-2.9" y="2.275" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$2" x="-2.9" y="1.625" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$3" x="-2.9" y="0.975" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$4" x="-2.9" y="0.325" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$5" x="-2.9" y="-0.325" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$6" x="-2.9" y="-0.975" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$7" x="-2.9" y="-1.625" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$8" x="-2.9" y="-2.275" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$9" x="2.9" y="-2.275" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$10" x="2.9" y="-1.625" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$11" x="2.9" y="-0.975" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$12" x="2.9" y="-0.325" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$13" x="2.9" y="0.325" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$14" x="2.9" y="0.975" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$15" x="2.9" y="1.625" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$16" x="2.9" y="2.275" dx="1.5" dy="0.2413" layer="1"/>
<smd name="P$17" x="0" y="0" dx="2.46" dy="2.46" layer="1"/>
<text x="-2.275" y="2.925" size="1.27" layer="21">&gt;NAME</text>
<circle x="-4.225" y="2.275" radius="0.325" width="0.127" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="DRV8428">
<pin name="VM" x="-17.78" y="17.78" length="middle"/>
<pin name="PGND" x="-17.78" y="12.7" length="middle"/>
<pin name="AOUT1" x="-17.78" y="7.62" length="middle"/>
<pin name="AOUT2" x="-17.78" y="2.54" length="middle"/>
<pin name="BOUT2" x="-17.78" y="-2.54" length="middle"/>
<pin name="BOUT1" x="-17.78" y="-7.62" length="middle"/>
<pin name="GND" x="-17.78" y="-12.7" length="middle"/>
<pin name="DVDD" x="-17.78" y="-17.78" length="middle"/>
<pin name="VREF" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="M0" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="DECAY" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="M1" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="STEP" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="DIR" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="EN" x="17.78" y="12.7" length="middle" rot="R180"/>
<pin name="NSLEEP" x="17.78" y="17.78" length="middle" rot="R180"/>
<pin name="TPAD" x="0" y="-27.94" length="middle" rot="R90"/>
<wire x1="-12.7" y1="22.86" x2="-12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-22.86" x2="12.7" y2="-22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="-22.86" x2="12.7" y2="22.86" width="0.254" layer="94"/>
<wire x1="12.7" y1="22.86" x2="-12.7" y2="22.86" width="0.254" layer="94"/>
<text x="-7.62" y="20.32" size="1.27" layer="94">&gt;NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DRV8428">
<gates>
<gate name="G$1" symbol="DRV8428" x="-2.54" y="5.08"/>
</gates>
<devices>
<device name="" package="DRV8428PWP">
<connects>
<connect gate="G$1" pin="AOUT1" pad="P$3"/>
<connect gate="G$1" pin="AOUT2" pad="P$4"/>
<connect gate="G$1" pin="BOUT1" pad="P$6"/>
<connect gate="G$1" pin="BOUT2" pad="P$5"/>
<connect gate="G$1" pin="DECAY" pad="P$11"/>
<connect gate="G$1" pin="DIR" pad="P$14"/>
<connect gate="G$1" pin="DVDD" pad="P$8"/>
<connect gate="G$1" pin="EN" pad="P$15"/>
<connect gate="G$1" pin="GND" pad="P$7"/>
<connect gate="G$1" pin="M0" pad="P$10"/>
<connect gate="G$1" pin="M1" pad="P$12"/>
<connect gate="G$1" pin="NSLEEP" pad="P$16"/>
<connect gate="G$1" pin="PGND" pad="P$2"/>
<connect gate="G$1" pin="STEP" pad="P$13"/>
<connect gate="G$1" pin="TPAD" pad="P$17"/>
<connect gate="G$1" pin="VM" pad="P$1"/>
<connect gate="G$1" pin="VREF" pad="P$9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="USB_MINIB">
<wire x1="-0.03" y1="3.8" x2="2.07" y2="3.8" width="0.2032" layer="21"/>
<wire x1="4.57" y1="3.1" x2="4.57" y2="2.2" width="0.2032" layer="21"/>
<wire x1="4.57" y1="-2.2" x2="4.57" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="2.07" y1="-3.8" x2="-0.03" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-4.63" y1="3.8" x2="-4.63" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-4.63" y1="-3.8" x2="-3.23" y2="-3.8" width="0.2032" layer="51"/>
<wire x1="-4.63" y1="3.8" x2="-3.23" y2="3.8" width="0.2032" layer="51"/>
<smd name="D+" x="3.77" y="0" dx="2.5" dy="0.35" layer="1"/>
<smd name="D-" x="3.77" y="0.8" dx="2.5" dy="0.35" layer="1"/>
<smd name="GND" x="3.77" y="-1.6" dx="2.5" dy="0.35" layer="1"/>
<smd name="ID" x="3.77" y="-0.8" dx="2.5" dy="0.35" layer="1"/>
<smd name="SHIELD@1" x="-1.73" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="SHIELD@2" x="-1.73" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="SHIELD@4" x="3.77" y="-4.5" dx="2.5" dy="2" layer="1"/>
<smd name="SHIELD@3" x="3.77" y="4.5" dx="2.5" dy="2" layer="1"/>
<smd name="VBUS" x="3.77" y="1.6" dx="2.5" dy="0.35" layer="1"/>
<text x="-2.54" y="1.27" size="0.4064" layer="25">&gt;NAME</text>
<text x="-2.54" y="0" size="0.4064" layer="27">&gt;VALUE</text>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206FAB">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="6MM_SWITCH">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<wire x1="3.302" y1="-0.762" x2="3.048" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.302" y2="0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="1.016" x2="3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.302" y1="0.762" x2="-3.048" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.302" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="3.048" y1="2.54" x2="2.54" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="-3.048" x2="3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="3.048" y1="-2.54" x2="3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="3.048" x2="-3.048" y2="2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="2.54" x2="-3.048" y2="1.016" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-3.048" y2="-2.54" width="0.1524" layer="51"/>
<wire x1="-3.048" y1="-2.54" x2="-3.048" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="-1.27" y2="-1.27" width="0.0508" layer="51"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="1.27" x2="1.27" y2="1.27" width="0.0508" layer="51"/>
<wire x1="-1.27" y1="3.048" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="-1.27" y2="2.794" width="0.0508" layer="21"/>
<wire x1="1.27" y1="2.794" x2="1.27" y2="3.048" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="-1.27" y2="-2.794" width="0.0508" layer="21"/>
<wire x1="1.143" y1="-2.794" x2="1.143" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="-1.27" y1="-2.794" x2="-1.27" y2="-3.048" width="0.0508" layer="21"/>
<wire x1="2.54" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.54" y1="-3.048" x2="-2.159" y2="-3.048" width="0.1524" layer="51"/>
<wire x1="-2.159" y1="-3.048" x2="-1.27" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.54" y1="3.048" x2="2.159" y2="3.048" width="0.1524" layer="51"/>
<wire x1="2.159" y1="3.048" x2="1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="1.27" y1="3.048" x2="-1.27" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="3.048" x2="-2.159" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-3.048" x2="1.143" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-3.048" x2="2.159" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="3.048" y1="-0.762" x2="3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="3.048" y1="0.762" x2="3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="-0.762" x2="-3.048" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-3.048" y1="0.762" x2="-3.048" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-2.159" x2="1.27" y2="-2.159" width="0.1524" layer="51"/>
<wire x1="1.27" y1="2.286" x2="-1.27" y2="2.286" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="1.27" x2="-2.413" y2="0.508" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="-0.508" x2="-2.413" y2="-1.27" width="0.1524" layer="51"/>
<wire x1="-2.413" y1="0.508" x2="-2.159" y2="-0.381" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="1.778" width="0.1524" layer="21"/>
<circle x="-2.159" y="-2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="-2.032" radius="0.508" width="0.1524" layer="51"/>
<circle x="2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="-2.159" y="2.159" radius="0.508" width="0.1524" layer="51"/>
<circle x="0" y="0" radius="0.635" width="0.0508" layer="51"/>
<circle x="0" y="0" radius="0.254" width="0.1524" layer="21"/>
<smd name="1" x="-3.302" y="2.286" dx="2.286" dy="1.524" layer="1"/>
<smd name="2" x="3.302" y="2.286" dx="2.286" dy="1.524" layer="1"/>
<smd name="3" x="-3.302" y="-2.286" dx="2.286" dy="1.524" layer="1"/>
<smd name="4" x="3.302" y="-2.286" dx="2.286" dy="1.524" layer="1"/>
<text x="-3.048" y="3.683" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.048" y="-5.08" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.318" y="1.651" size="1.27" layer="51" ratio="10">1</text>
<text x="3.556" y="1.524" size="1.27" layer="51" ratio="10">2</text>
<text x="-4.572" y="-2.794" size="1.27" layer="51" ratio="10">3</text>
<text x="3.556" y="-2.794" size="1.27" layer="51" ratio="10">4</text>
</package>
<package name="C1206">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="C1206FAB">
<wire x1="-2.032" y1="1.016" x2="2.032" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="1.016" x2="2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.016" x2="-2.032" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-2.032" y1="-1.016" x2="-2.032" y2="1.016" width="0.127" layer="21"/>
<smd name="1" x="-1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<smd name="2" x="1.651" y="0" dx="1.27" dy="1.905" layer="1"/>
<text x="-1.778" y="1.27" size="1.016" layer="25" ratio="15">&gt;NAME</text>
<text x="-1.778" y="-2.286" size="1.016" layer="27" ratio="15">&gt;VALUE</text>
</package>
<package name="C2220">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-3.743" y1="2.253" x2="3.743" y2="2.253" width="0.0508" layer="39"/>
<wire x1="3.743" y1="-2.253" x2="-3.743" y2="-2.253" width="0.0508" layer="39"/>
<wire x1="-3.743" y1="-2.253" x2="-3.743" y2="2.253" width="0.0508" layer="39"/>
<wire x1="3.743" y1="2.253" x2="3.743" y2="-2.253" width="0.0508" layer="39"/>
<smd name="1" x="-2.794" y="0" dx="2.032" dy="5.334" layer="1"/>
<smd name="2" x="2.794" y="0" dx="2.032" dy="5.334" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.9718" y1="-0.8509" x2="-2.2217" y2="0.8491" layer="51"/>
<rectangle x1="2.2217" y1="-0.8491" x2="2.9718" y2="0.8509" layer="51"/>
</package>
<package name="SOT223">
<description>&lt;b&gt;SOT-223&lt;/b&gt;</description>
<wire x1="3.2766" y1="1.651" x2="3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="3.2766" y1="-1.651" x2="-3.2766" y2="-1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="-1.651" x2="-3.2766" y2="1.651" width="0.2032" layer="21"/>
<wire x1="-3.2766" y1="1.651" x2="3.2766" y2="1.651" width="0.2032" layer="21"/>
<smd name="1" x="-2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="2" x="0" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="3" x="2.3114" y="-3.0988" dx="1.2192" dy="2.2352" layer="1"/>
<smd name="4" x="0" y="3.099" dx="3.6" dy="2.2" layer="1" thermals="no"/>
<text x="-0.8255" y="4.5085" size="0.4064" layer="25">&gt;NAME</text>
<text x="-1.0795" y="-0.1905" size="0.4064" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
<rectangle x1="-1.6002" y1="1.8034" x2="1.6002" y2="3.6576" layer="51"/>
<rectangle x1="-0.4318" y1="-3.6576" x2="0.4318" y2="-1.8034" layer="51"/>
<rectangle x1="-2.7432" y1="-3.6576" x2="-1.8796" y2="-1.8034" layer="51"/>
<rectangle x1="1.8796" y1="-3.6576" x2="2.7432" y2="-1.8034" layer="51"/>
</package>
<package name="P-LCC-4">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="51">R</text>
<text x="-1.905" y="2.54" size="1.27" layer="51">G</text>
<text x="1.27" y="2.54" size="1.27" layer="51">B</text>
<text x="1.27" y="-3.81" size="1.27" layer="51">A</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
<smd name="A" x="0.9016" y="-1.5" dx="1.1" dy="1.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="R" x="-0.9016" y="-1.5" dx="1.1" dy="1.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="G" x="-0.9016" y="1.5" dx="1.1" dy="1.5" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="B" x="0.9016" y="1.5" dx="1.1" dy="1.5" layer="1" rot="R180" stop="no" cream="no"/>
<circle x="-1.905" y="-1.524" radius="0.127" width="0.127" layer="21"/>
</package>
<package name="P-LCC-4-FANCYFAB">
<description>&lt;b&gt;Power TOPLED®&lt;/b&gt;&lt;p&gt;
Source: http://www.osram.convergy.de/ ... LA_LO_LA_LY E67B.pdf</description>
<wire x1="-1.4" y1="-1.05" x2="-1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="-1.6" x2="-1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-0.85" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="1" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1" y1="-1.6" x2="1.4" y2="-1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="-1.6" x2="1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.4" y1="1.6" x2="1.1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="1.1" y1="1.6" x2="-1" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1.4" y2="1.6" width="0.2032" layer="51"/>
<wire x1="-1" y1="1.6" x2="-1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="1.8" x2="-0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="1.8" x2="-0.5" y2="1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.65" x2="0.5" y2="1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="1.8" x2="1.1" y2="1.8" width="0.1016" layer="51"/>
<wire x1="1.1" y1="1.8" x2="1.1" y2="1.6" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.6" x2="-1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-1" y1="-1.8" x2="-0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="-0.5" y1="-1.8" x2="-0.5" y2="-1.65" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.65" x2="0.5" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-1.8" x2="1" y2="-1.8" width="0.1016" layer="51"/>
<wire x1="1" y1="-1.8" x2="1" y2="-1.6" width="0.1016" layer="51"/>
<wire x1="-0.85" y1="-1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<wire x1="-1.4" y1="1.6" x2="-1.4" y2="-1.05" width="0.2032" layer="51"/>
<circle x="0" y="0" radius="1.1" width="0.2032" layer="51"/>
<text x="-3.81" y="-2.54" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-2.54" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-3.81" size="1.27" layer="51">R</text>
<text x="-1.905" y="2.54" size="1.27" layer="51">G</text>
<text x="1.27" y="2.54" size="1.27" layer="51">B</text>
<text x="1.27" y="-3.81" size="1.27" layer="51">A</text>
<rectangle x1="-1.15" y1="0.75" x2="-0.35" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="0.75" x2="1.15" y2="1.85" layer="29"/>
<rectangle x1="0.35" y1="-1.85" x2="1.15" y2="-0.75" layer="29"/>
<rectangle x1="-1.15" y1="-1.85" x2="-0.35" y2="-0.75" layer="29"/>
<rectangle x1="-1.1" y1="-1.8" x2="-0.4" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="-1.8" x2="1.1" y2="-0.8" layer="31"/>
<rectangle x1="0.4" y1="0.8" x2="1.1" y2="1.8" layer="31"/>
<rectangle x1="-1.1" y1="0.8" x2="-0.4" y2="1.8" layer="31"/>
<rectangle x1="-0.2" y1="-0.2" x2="0.2" y2="0.2" layer="21"/>
<smd name="A" x="0.75" y="-1.3" dx="0.8" dy="1.1" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="R" x="-0.75" y="-1.3" dx="0.8" dy="1.1" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="G" x="-0.75" y="1.3" dx="0.8" dy="1.1" layer="1" rot="R180" stop="no" cream="no"/>
<smd name="B" x="0.75" y="1.3" dx="0.8" dy="1.1" layer="1" rot="R180" stop="no" cream="no"/>
<circle x="-1.705" y="-1.524" radius="0.127" width="0.127" layer="21"/>
</package>
<package name="2X02SMD">
<smd name="1" x="-2.54" y="1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="3" x="-2.54" y="-1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="2" x="2.92" y="1.27" dx="2.54" dy="1.27" layer="1"/>
<smd name="4" x="2.92" y="-1.27" dx="2.54" dy="1.27" layer="1"/>
<text x="-5.08" y="1.27" size="1.27" layer="27">1</text>
<text x="-3.81" y="2.54" size="1.27" layer="21">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="21">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="USB">
<wire x1="5.08" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-7.62" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<text x="3.81" y="-5.08" size="2.54" layer="94" rot="R90">USB</text>
<pin name="D+" x="-2.54" y="2.54" visible="pad" length="short"/>
<pin name="D-" x="-2.54" y="0" visible="pad" length="short"/>
<pin name="VBUS" x="-2.54" y="-2.54" visible="pad" length="short"/>
<pin name="GND" x="-2.54" y="-5.08" visible="pad" length="short"/>
<pin name="SHIELD" x="2.54" y="-10.16" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="R-US">
<wire x1="-2.54" y1="0" x2="-2.159" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-2.159" y1="1.016" x2="-1.524" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-1.524" y1="-1.016" x2="-0.889" y2="1.016" width="0.2032" layer="94"/>
<wire x1="-0.889" y1="1.016" x2="-0.254" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="-0.254" y1="-1.016" x2="0.381" y2="1.016" width="0.2032" layer="94"/>
<wire x1="0.381" y1="1.016" x2="1.016" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="1.016" y1="-1.016" x2="1.651" y2="1.016" width="0.2032" layer="94"/>
<wire x1="1.651" y1="1.016" x2="2.286" y2="-1.016" width="0.2032" layer="94"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="0" width="0.2032" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="TS2">
<wire x1="0" y1="1.905" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-3.175" y2="1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="-1.905" x2="-3.175" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-4.445" y1="1.905" x2="-4.445" y2="0" width="0.254" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.905" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.635" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.175" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-2.54" radius="0.127" width="0.4064" layer="94"/>
<circle x="0" y="2.54" radius="0.127" width="0.4064" layer="94"/>
<text x="-6.35" y="-2.54" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.81" y="3.175" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="S1" x="2.54" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="P1" x="2.54" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="2" rot="R90"/>
</symbol>
<symbol name="CAP-NONPOLARIZED">
<description>non-polarized capacitor</description>
<wire x1="-1.778" y1="1.524" x2="-1.778" y2="0" width="0.254" layer="94"/>
<wire x1="-1.778" y1="0" x2="-1.778" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-0.762" y1="1.524" x2="-0.762" y2="0" width="0.254" layer="94"/>
<wire x1="-0.762" y1="0" x2="-0.762" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.778" y2="0" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<text x="-3.81" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="2.54" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
<symbol name="REGULATOR_SOT223">
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-3.81" x2="0" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="0" y1="-3.81" x2="6.35" y2="-3.81" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-3.81" x2="6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-6.35" y="-6.35" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="-6.35" size="1.27" layer="96">&gt;VALUE</text>
<pin name="IN" x="-7.62" y="2.54" length="point"/>
<pin name="GND" x="0" y="-5.08" length="point" rot="R90"/>
<pin name="OUT" x="7.62" y="2.54" length="point" rot="R180"/>
</symbol>
<symbol name="LED-RGB">
<wire x1="6.35" y1="1.778" x2="5.08" y2="-0.254" width="0.254" layer="94"/>
<wire x1="5.08" y1="-0.254" x2="3.81" y2="1.778" width="0.254" layer="94"/>
<wire x1="6.35" y1="-0.254" x2="5.08" y2="-0.254" width="0.254" layer="94"/>
<wire x1="5.08" y1="-0.254" x2="3.81" y2="-0.254" width="0.254" layer="94"/>
<wire x1="6.35" y1="1.778" x2="3.81" y2="1.778" width="0.254" layer="94"/>
<wire x1="3.048" y1="1.524" x2="2.159" y2="0.635" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0.762" x2="2.921" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.778" x2="0" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<wire x1="1.27" y1="-0.254" x2="0" y2="-0.254" width="0.254" layer="94"/>
<wire x1="0" y1="-0.254" x2="-1.27" y2="-0.254" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.778" x2="-1.27" y2="1.778" width="0.254" layer="94"/>
<wire x1="-2.032" y1="1.524" x2="-2.921" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.762" x2="-2.159" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="1.778" x2="-5.08" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-0.254" x2="-6.35" y2="1.778" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.254" x2="-5.08" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-0.254" x2="-6.35" y2="-0.254" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.778" x2="-6.35" y2="1.778" width="0.254" layer="94"/>
<wire x1="-7.112" y1="1.524" x2="-8.001" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="0.762" x2="-7.239" y2="-0.127" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-5.08" y="2.54" radius="0.1796" width="0.254" layer="94"/>
<circle x="0" y="2.54" radius="0.1796" width="0.254" layer="94"/>
<text x="-2.54" y="3.302" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="5.461" size="1.778" layer="96">&gt;VALUE</text>
<pin name="CGREEN" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="CBLUE" x="0" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="CRED" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="A2" x="-5.08" y="5.08" visible="pad" length="middle" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="2.032" y="1.016"/>
<vertex x="1.778" y="0.254"/>
<vertex x="2.54" y="0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="2.794" y="0.254"/>
<vertex x="2.54" y="-0.508"/>
<vertex x="3.302" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.048" y="1.016"/>
<vertex x="-3.302" y="0.254"/>
<vertex x="-2.54" y="0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-2.286" y="0.254"/>
<vertex x="-2.54" y="-0.508"/>
<vertex x="-1.778" y="-0.254"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-8.128" y="1.016"/>
<vertex x="-8.382" y="0.254"/>
<vertex x="-7.62" y="0.508"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-7.366" y="0.254"/>
<vertex x="-7.62" y="-0.508"/>
<vertex x="-6.858" y="-0.254"/>
</polygon>
</symbol>
<symbol name="PINH2X2">
<wire x1="-8.89" y1="-2.54" x2="6.35" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-8.89" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-8.89" y1="5.08" x2="-8.89" y2="-2.54" width="0.4064" layer="94"/>
<text x="-8.89" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-8.89" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-5.08" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="2.54" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_USB_MINIB">
<gates>
<gate name="G$1" symbol="USB" x="0" y="0"/>
</gates>
<devices>
<device name="" package="USB_MINIB">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="SHIELD" pad="SHIELD@1 SHIELD@2 SHIELD@3 SHIELD@4"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R" prefix="R" uservalue="yes">
<description>&lt;b&gt;Resistor (US Symbol)&lt;/b&gt;
&lt;p&gt;
Variants with postfix FAB are widened to allow the routing of internal traces</description>
<gates>
<gate name="G$1" symbol="R-US" x="0" y="0"/>
</gates>
<devices>
<device name="1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206FAB" package="R1206FAB">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW_SWITCH_TACTILE_6MM" prefix="S" uservalue="yes">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<gates>
<gate name="2" symbol="TS2" x="0" y="0"/>
</gates>
<devices>
<device name="6MM_SWITCH" package="6MM_SWITCH">
<connects>
<connect gate="2" pin="P" pad="1"/>
<connect gate="2" pin="P1" pad="2"/>
<connect gate="2" pin="S" pad="3"/>
<connect gate="2" pin="S1" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAP_UNPOLARIZED" prefix="C" uservalue="yes">
<gates>
<gate name="&gt;NAME" symbol="CAP-NONPOLARIZED" x="0" y="0"/>
</gates>
<devices>
<device name="" package="C1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FAB" package="C1206FAB">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="2220" package="C2220">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VR_REGULATOR_SOT223" prefix="U">
<gates>
<gate name="G$1" symbol="REGULATOR_SOT223" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SOT223">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IN" pad="3"/>
<connect gate="G$1" pin="OUT" pad="2 4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED_RGBNEW" prefix="D">
<gates>
<gate name="G$1" symbol="LED-RGB" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="P-LCC-4">
<connects>
<connect gate="G$1" pin="A2" pad="A"/>
<connect gate="G$1" pin="CBLUE" pad="B"/>
<connect gate="G$1" pin="CGREEN" pad="G"/>
<connect gate="G$1" pin="CRED" pad="R"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="FANCYFAB" package="P-LCC-4-FANCYFAB">
<connects>
<connect gate="G$1" pin="A2" pad="A"/>
<connect gate="G$1" pin="CBLUE" pad="B"/>
<connect gate="G$1" pin="CGREEN" pad="G"/>
<connect gate="G$1" pin="CRED" pad="R"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_02X2-PINHEAD-SMD">
<gates>
<gate name="G$1" symbol="PINH2X2" x="2.54" y="0"/>
</gates>
<devices>
<device name="" package="2X02SMD">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="con-leotronics" urn="urn:adsk.eagle:library:160">
<description>&lt;b&gt;Connectors from Leotronics&lt;/b&gt;&lt;p&gt;
www.leotronics.co.uk&lt;br&gt;</description>
<packages>
<package name="1365-10" urn="urn:adsk.eagle:footprint:7867/1" library_version="2">
<description>&lt;b&gt;DUAL ROW STRAIGHT HEADER&lt;/b&gt; 1364 SERIES. 1.27mm x 1.27mm. Without Fixing Post. SMT&lt;p&gt;
Source: http://www.leotronics.co.uk/Conexcon/Data%20Sheets/sec.%20A/1364ing.pdf</description>
<wire x1="-3.075" y1="1.63" x2="3.07" y2="1.63" width="0.2032" layer="21"/>
<wire x1="3.07" y1="1.63" x2="3.07" y2="-1.63" width="0.2032" layer="21"/>
<wire x1="3.07" y1="-1.63" x2="-3.075" y2="-1.63" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.63" x2="-3.075" y2="1.63" width="0.2032" layer="21"/>
<smd name="1" x="-2.54" y="-2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="2" x="-2.54" y="2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="3" x="-1.27" y="-2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="4" x="-1.27" y="2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="5" x="0" y="-2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="6" x="0" y="2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="7" x="1.27" y="-2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="8" x="1.27" y="2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="9" x="2.54" y="-2.95" dx="0.76" dy="2.4" layer="1"/>
<smd name="10" x="2.54" y="2.95" dx="0.76" dy="2.4" layer="1"/>
<text x="-3.81" y="-3.175" size="1.27" layer="25" rot="R90">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.27" layer="27" rot="R90">&gt;VALUE</text>
<rectangle x1="-2.74" y1="1.725" x2="-2.34" y2="2.75" layer="51"/>
<rectangle x1="-2.74" y1="-2.75" x2="-2.34" y2="-1.725" layer="51" rot="R180"/>
<rectangle x1="-1.47" y1="1.725" x2="-1.07" y2="2.75" layer="51"/>
<rectangle x1="-1.47" y1="-2.75" x2="-1.07" y2="-1.725" layer="51" rot="R180"/>
<rectangle x1="-0.2" y1="1.725" x2="0.2" y2="2.75" layer="51"/>
<rectangle x1="-0.2" y1="-2.75" x2="0.2" y2="-1.725" layer="51" rot="R180"/>
<rectangle x1="1.07" y1="1.725" x2="1.47" y2="2.75" layer="51"/>
<rectangle x1="1.07" y1="-2.75" x2="1.47" y2="-1.725" layer="51" rot="R180"/>
<rectangle x1="2.34" y1="1.725" x2="2.74" y2="2.75" layer="51"/>
<rectangle x1="2.34" y1="-2.75" x2="2.74" y2="-1.725" layer="51" rot="R180"/>
</package>
</packages>
<packages3d>
<package3d name="1365-10" urn="urn:adsk.eagle:package:7957/1" type="box" library_version="2">
<description>DUAL ROW STRAIGHT HEADER 1364 SERIES. 1.27mm x 1.27mm. Without Fixing Post. SMT
Source: http://www.leotronics.co.uk/Conexcon/Data%20Sheets/sec.%20A/1364ing.pdf</description>
<packageinstances>
<packageinstance name="1365-10"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MVAL" urn="urn:adsk.eagle:symbol:7864/1" library_version="2">
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="1.651" size="1.778" layer="96">&gt;VALUE</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="M" urn="urn:adsk.eagle:symbol:7865/1" library_version="2">
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<text x="2.54" y="-0.762" size="1.778" layer="95">&gt;NAME</text>
<pin name="S" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1365-10" urn="urn:adsk.eagle:component:8049/2" prefix="X" library_version="2">
<description>&lt;b&gt;DUAL ROW STRAIGHT HEADER&lt;/b&gt; 1364 SERIES. 1.27mm x 1.27mm. Without Fixing Post. SMT&lt;p&gt;
Source: http://www.leotronics.co.uk/Conexcon/Data%20Sheets/sec.%20A/1364ing.pdf</description>
<gates>
<gate name="-1" symbol="MVAL" x="0" y="0" addlevel="always" swaplevel="1"/>
<gate name="-2" symbol="MVAL" x="30.48" y="0" addlevel="always" swaplevel="1"/>
<gate name="-3" symbol="M" x="0" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-4" symbol="M" x="30.48" y="-2.54" addlevel="always" swaplevel="1"/>
<gate name="-5" symbol="M" x="0" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-6" symbol="M" x="30.48" y="-5.08" addlevel="always" swaplevel="1"/>
<gate name="-7" symbol="M" x="0" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-8" symbol="M" x="30.48" y="-7.62" addlevel="always" swaplevel="1"/>
<gate name="-9" symbol="M" x="0" y="-10.16" addlevel="always" swaplevel="1"/>
<gate name="-10" symbol="M" x="30.48" y="-10.16" addlevel="always" swaplevel="1"/>
</gates>
<devices>
<device name="" package="1365-10">
<connects>
<connect gate="-1" pin="S" pad="1"/>
<connect gate="-10" pin="S" pad="10"/>
<connect gate="-2" pin="S" pad="2"/>
<connect gate="-3" pin="S" pad="3"/>
<connect gate="-4" pin="S" pad="4"/>
<connect gate="-5" pin="S" pad="5"/>
<connect gate="-6" pin="S" pad="6"/>
<connect gate="-7" pin="S" pad="7"/>
<connect gate="-8" pin="S" pad="8"/>
<connect gate="-9" pin="S" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:7957/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="MF" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OC_FARNELL" value="unknown" constant="no"/>
<attribute name="OC_NEWARK" value="unknown" constant="no"/>
<attribute name="POPULARITY" value="1" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="ATSAMD21E18A-AUT" deviceset="ATSAMD21E18A-AUT" device=""/>
<part name="U$1" library="DRV8428" deviceset="DRV8428" device=""/>
<part name="U$2" library="fab" deviceset="CONN_USB_MINIB" device=""/>
<part name="R1" library="fab" deviceset="R" device="1206FAB"/>
<part name="S1" library="fab" deviceset="SW_SWITCH_TACTILE_6MM" device="6MM_SWITCH"/>
<part name="BIGBOYCAP" library="fab" deviceset="CAP_UNPOLARIZED" device="2220"/>
<part name="C2" library="fab" deviceset="CAP_UNPOLARIZED" device="FAB"/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U2" library="fab" deviceset="VR_REGULATOR_SOT223" device=""/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D1" library="fab" deviceset="LED_RGBNEW" device=""/>
<part name="R2" library="fab" deviceset="R" device="1206FAB"/>
<part name="R3" library="fab" deviceset="R" device="1206FAB"/>
<part name="R4" library="fab" deviceset="R" device="1206FAB"/>
<part name="C1" library="fab" deviceset="CAP_UNPOLARIZED" device="FAB"/>
<part name="GND4" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND5" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND6" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="U$3" library="fab" deviceset="CONN_02X2-PINHEAD-SMD" device=""/>
<part name="U$4" library="fab" deviceset="CONN_02X2-PINHEAD-SMD" device=""/>
<part name="GND7" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND8" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="C3" library="fab" deviceset="CAP_UNPOLARIZED" device="FAB"/>
<part name="GND9" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND10" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="GND11" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R5" library="fab" deviceset="R" device="1206FAB" value="100K"/>
<part name="R6" library="fab" deviceset="R" device="1206FAB"/>
<part name="X1" library="con-leotronics" library_urn="urn:adsk.eagle:library:160" deviceset="1365-10" device="" package3d_urn="urn:adsk.eagle:package:7957/1"/>
<part name="GND12" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="R7" library="fab" deviceset="R" device="1206FAB"/>
<part name="R8" library="fab" deviceset="R" device="1206FAB"/>
<part name="GND13" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="40.64" y="27.94" smashed="yes">
<attribute name="NAME" x="27.94" y="54.61" size="1.778" layer="95"/>
<attribute name="VALUE" x="27.94" y="-2.54" size="1.778" layer="96"/>
</instance>
<instance part="U$1" gate="G$1" x="114.3" y="30.48" smashed="yes">
<attribute name="NAME" x="106.68" y="50.8" size="1.27" layer="94"/>
</instance>
<instance part="U$2" gate="G$1" x="73.66" y="7.62" smashed="yes"/>
<instance part="R1" gate="G$1" x="15.24" y="50.8" smashed="yes" rot="R90">
<attribute name="NAME" x="13.7414" y="46.99" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="18.542" y="46.99" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="S1" gate="2" x="10.16" y="38.1" smashed="yes">
<attribute name="NAME" x="3.81" y="35.56" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="6.35" y="41.275" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="BIGBOYCAP" gate="&gt;NAME" x="91.44" y="60.96" smashed="yes">
<attribute name="NAME" x="87.63" y="63.5" size="1.778" layer="95"/>
<attribute name="VALUE" x="87.63" y="57.15" size="1.778" layer="96"/>
</instance>
<instance part="C2" gate="&gt;NAME" x="91.44" y="55.88" smashed="yes">
<attribute name="NAME" x="87.63" y="58.42" size="1.778" layer="95"/>
<attribute name="VALUE" x="87.63" y="52.07" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="5.08" y="30.48" smashed="yes">
<attribute name="VALUE" x="2.54" y="27.94" size="1.778" layer="96"/>
</instance>
<instance part="U2" gate="G$1" x="33.02" y="114.3" smashed="yes">
<attribute name="NAME" x="26.67" y="107.95" size="1.27" layer="95"/>
<attribute name="VALUE" x="34.29" y="107.95" size="1.27" layer="96"/>
</instance>
<instance part="GND2" gate="1" x="33.02" y="99.06" smashed="yes">
<attribute name="VALUE" x="30.48" y="96.52" size="1.778" layer="96"/>
</instance>
<instance part="GND3" gate="1" x="71.12" y="-2.54" smashed="yes">
<attribute name="VALUE" x="68.58" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="-2.54" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="-5.842" y="15.24" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-8.001" y="15.24" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R2" gate="G$1" x="7.62" y="22.86" smashed="yes" rot="R180">
<attribute name="NAME" x="11.43" y="21.3614" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="11.43" y="26.162" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="7.62" y="17.78" smashed="yes" rot="R180">
<attribute name="NAME" x="11.43" y="16.2814" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="11.43" y="21.082" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R4" gate="G$1" x="7.62" y="12.7" smashed="yes" rot="R180">
<attribute name="NAME" x="11.43" y="11.2014" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="11.43" y="16.002" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="C1" gate="&gt;NAME" x="63.5" y="50.8" smashed="yes">
<attribute name="NAME" x="59.69" y="53.34" size="1.778" layer="95"/>
<attribute name="VALUE" x="59.69" y="46.99" size="1.778" layer="96"/>
</instance>
<instance part="GND4" gate="1" x="71.12" y="48.26" smashed="yes">
<attribute name="VALUE" x="68.58" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="GND5" gate="1" x="58.42" y="-2.54" smashed="yes">
<attribute name="VALUE" x="55.88" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="GND6" gate="1" x="114.3" y="-5.08" smashed="yes">
<attribute name="VALUE" x="111.76" y="-7.62" size="1.778" layer="96"/>
</instance>
<instance part="U$3" gate="G$1" x="83.82" y="33.02" smashed="yes" rot="R90">
<attribute name="NAME" x="78.105" y="24.13" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="88.9" y="24.13" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="U$4" gate="G$1" x="88.9" y="71.12" smashed="yes">
<attribute name="NAME" x="80.01" y="76.835" size="1.778" layer="95"/>
<attribute name="VALUE" x="80.01" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="GND7" gate="1" x="104.14" y="68.58" smashed="yes">
<attribute name="VALUE" x="101.6" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="GND8" gate="1" x="144.78" y="20.32" smashed="yes">
<attribute name="VALUE" x="142.24" y="17.78" size="1.778" layer="96"/>
</instance>
<instance part="C3" gate="&gt;NAME" x="88.9" y="7.62" smashed="yes" rot="R90">
<attribute name="NAME" x="86.36" y="3.81" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="92.71" y="3.81" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND9" gate="1" x="83.82" y="53.34" smashed="yes">
<attribute name="VALUE" x="81.28" y="50.8" size="1.778" layer="96"/>
</instance>
<instance part="GND10" gate="1" x="88.9" y="-2.54" smashed="yes">
<attribute name="VALUE" x="86.36" y="-5.08" size="1.778" layer="96"/>
</instance>
<instance part="GND11" gate="1" x="20.32" y="81.28" smashed="yes">
<attribute name="VALUE" x="17.78" y="78.74" size="1.778" layer="96"/>
</instance>
<instance part="R5" gate="G$1" x="-7.62" y="66.04" smashed="yes" rot="R90">
<attribute name="NAME" x="-9.1186" y="62.23" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-4.318" y="62.23" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R6" gate="G$1" x="-7.62" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="-9.1186" y="77.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-4.318" y="77.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="X1" gate="-1" x="20.32" y="144.78" smashed="yes">
<attribute name="NAME" x="22.86" y="144.018" size="1.778" layer="95"/>
<attribute name="VALUE" x="17.78" y="146.431" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="-2" x="50.8" y="144.78" smashed="yes">
<attribute name="NAME" x="53.34" y="144.018" size="1.778" layer="95"/>
<attribute name="VALUE" x="48.26" y="146.431" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="-3" x="20.32" y="142.24" smashed="yes">
<attribute name="NAME" x="22.86" y="141.478" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-4" x="50.8" y="142.24" smashed="yes">
<attribute name="NAME" x="53.34" y="141.478" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-5" x="20.32" y="139.7" smashed="yes">
<attribute name="NAME" x="22.86" y="138.938" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-6" x="50.8" y="139.7" smashed="yes">
<attribute name="NAME" x="53.34" y="138.938" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-7" x="20.32" y="137.16" smashed="yes">
<attribute name="NAME" x="22.86" y="136.398" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-8" x="50.8" y="137.16" smashed="yes">
<attribute name="NAME" x="53.34" y="136.398" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-9" x="20.32" y="134.62" smashed="yes">
<attribute name="NAME" x="22.86" y="133.858" size="1.778" layer="95"/>
</instance>
<instance part="X1" gate="-10" x="50.8" y="134.62" smashed="yes">
<attribute name="NAME" x="53.34" y="133.858" size="1.778" layer="95"/>
</instance>
<instance part="GND12" gate="1" x="10.16" y="139.7" smashed="yes">
<attribute name="VALUE" x="7.62" y="137.16" size="1.778" layer="96"/>
</instance>
<instance part="R7" gate="G$1" x="-25.4" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="-26.8986" y="77.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-22.098" y="77.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R8" gate="G$1" x="-30.48" y="81.28" smashed="yes" rot="R90">
<attribute name="NAME" x="-31.9786" y="77.47" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="-27.178" y="77.47" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="GND13" gate="1" x="-27.94" y="73.66" smashed="yes">
<attribute name="VALUE" x="-30.48" y="71.12" size="1.778" layer="96"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="S1" gate="2" pin="P1"/>
<pinref part="S1" gate="2" pin="P"/>
<wire x1="12.7" y1="33.02" x2="10.16" y2="33.02" width="0.1524" layer="91"/>
<wire x1="10.16" y1="33.02" x2="5.08" y2="33.02" width="0.1524" layer="91"/>
<junction x="10.16" y="33.02"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U2" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="109.22" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="GND"/>
<wire x1="71.12" y1="2.54" x2="71.12" y2="0" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="C1" gate="&gt;NAME" pin="2"/>
<wire x1="66.04" y1="50.8" x2="71.12" y2="50.8" width="0.1524" layer="91"/>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="71.12" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
<junction x="71.12" y="50.8"/>
<pinref part="U$1" gate="G$1" pin="PGND"/>
<wire x1="96.52" y1="43.18" x2="73.66" y2="43.18" width="0.1524" layer="91"/>
<wire x1="73.66" y1="43.18" x2="73.66" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="58.42" y1="2.54" x2="58.42" y2="0" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="17.78" x2="114.3" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U$1" gate="G$1" pin="TPAD"/>
<wire x1="114.3" y1="17.78" x2="114.3" y2="2.54" width="0.1524" layer="91"/>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="114.3" y1="2.54" x2="114.3" y2="-2.54" width="0.1524" layer="91"/>
<junction x="114.3" y="2.54"/>
</segment>
<segment>
<pinref part="U$4" gate="G$1" pin="4"/>
<wire x1="91.44" y1="71.12" x2="104.14" y2="71.12" width="0.1524" layer="91"/>
<pinref part="GND7" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="DECAY"/>
<wire x1="132.08" y1="22.86" x2="144.78" y2="22.86" width="0.1524" layer="91"/>
<pinref part="GND8" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="BIGBOYCAP" gate="&gt;NAME" pin="1"/>
<pinref part="C2" gate="&gt;NAME" pin="1"/>
<wire x1="86.36" y1="60.96" x2="86.36" y2="55.88" width="0.1524" layer="91"/>
<wire x1="86.36" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<wire x1="83.82" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
<wire x1="81.28" y1="55.88" x2="81.28" y2="58.42" width="0.1524" layer="91"/>
<junction x="86.36" y="55.88"/>
<pinref part="GND9" gate="1" pin="GND"/>
<junction x="83.82" y="55.88"/>
</segment>
<segment>
<pinref part="GND10" gate="1" pin="GND"/>
<pinref part="C3" gate="&gt;NAME" pin="1"/>
<wire x1="88.9" y1="0" x2="88.9" y2="2.54" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="25.4" y1="81.28" x2="25.4" y2="83.82" width="0.1524" layer="91"/>
<wire x1="25.4" y1="83.82" x2="20.32" y2="83.82" width="0.1524" layer="91"/>
<pinref part="GND11" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="X1" gate="-5" pin="S"/>
<pinref part="X1" gate="-3" pin="S"/>
<wire x1="17.78" y1="139.7" x2="17.78" y2="142.24" width="0.1524" layer="91"/>
<wire x1="17.78" y1="142.24" x2="10.16" y2="142.24" width="0.1524" layer="91"/>
<junction x="17.78" y="142.24"/>
<pinref part="GND12" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="R8" gate="G$1" pin="1"/>
<pinref part="R7" gate="G$1" pin="1"/>
<wire x1="-30.48" y1="76.2" x2="-27.94" y2="76.2" width="0.1524" layer="91"/>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="-27.94" y1="76.2" x2="-25.4" y2="76.2" width="0.1524" layer="91"/>
<junction x="-27.94" y="76.2"/>
<pinref part="R8" gate="G$1" pin="2"/>
<pinref part="R7" gate="G$1" pin="2"/>
<wire x1="-30.48" y1="86.36" x2="-27.94" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="86.36" x2="-25.4" y2="86.36" width="0.1524" layer="91"/>
<wire x1="-27.94" y1="76.2" x2="-27.94" y2="86.36" width="0.1524" layer="91"/>
<junction x="-27.94" y="86.36"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA24"/>
<wire x1="58.42" y1="20.32" x2="66.04" y2="20.32" width="0.1524" layer="91"/>
<wire x1="66.04" y1="20.32" x2="66.04" y2="7.62" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="D-"/>
<wire x1="66.04" y1="7.62" x2="71.12" y2="7.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="D+"/>
<wire x1="71.12" y1="10.16" x2="71.12" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PA25"/>
<wire x1="71.12" y1="17.78" x2="58.42" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="IN"/>
<wire x1="25.4" y1="116.84" x2="17.78" y2="116.84" width="0.1524" layer="91"/>
<label x="20.32" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$2" gate="G$1" pin="VBUS"/>
<wire x1="71.12" y1="5.08" x2="63.5" y2="5.08" width="0.1524" layer="91"/>
<label x="63.5" y="5.08" size="1.778" layer="95"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="U2" gate="G$1" pin="OUT"/>
<wire x1="40.64" y1="116.84" x2="50.8" y2="116.84" width="0.1524" layer="91"/>
<label x="45.72" y="116.84" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R1" gate="G$1" pin="2"/>
<wire x1="15.24" y1="55.88" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<label x="15.24" y="60.96" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="D1" gate="G$1" pin="A2"/>
<wire x1="-7.62" y1="12.7" x2="-12.7" y2="12.7" width="0.1524" layer="91"/>
<label x="-10.16" y="12.7" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="VDDANA"/>
<pinref part="U1" gate="G$1" pin="VDDIN"/>
<wire x1="58.42" y1="45.72" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDCORE"/>
<wire x1="58.42" y1="48.26" x2="58.42" y2="50.8" width="0.1524" layer="91"/>
<junction x="58.42" y="48.26"/>
<wire x1="58.42" y1="50.8" x2="58.42" y2="60.96" width="0.1524" layer="91"/>
<junction x="58.42" y="50.8"/>
<label x="58.42" y="58.42" size="1.778" layer="95"/>
<pinref part="C1" gate="&gt;NAME" pin="1"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="EN"/>
<pinref part="U$1" gate="G$1" pin="NSLEEP"/>
<wire x1="132.08" y1="43.18" x2="132.08" y2="48.26" width="0.1524" layer="91"/>
<wire x1="132.08" y1="48.26" x2="139.7" y2="48.26" width="0.1524" layer="91"/>
<junction x="132.08" y="48.26"/>
<label x="137.16" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="25.4" y1="86.36" x2="20.32" y2="86.36" width="0.1524" layer="91"/>
<wire x1="20.32" y1="86.36" x2="20.32" y2="91.44" width="0.1524" layer="91"/>
<label x="20.32" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-1" pin="S"/>
<wire x1="17.78" y1="144.78" x2="12.7" y2="144.78" width="0.1524" layer="91"/>
<wire x1="12.7" y1="144.78" x2="12.7" y2="149.86" width="0.1524" layer="91"/>
<label x="12.7" y="149.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="1"/>
<pinref part="U1" gate="G$1" pin="PA06"/>
<wire x1="22.86" y1="22.86" x2="12.7" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="R3" gate="G$1" pin="1"/>
<wire x1="20.32" y1="20.32" x2="20.32" y2="17.78" width="0.1524" layer="91"/>
<wire x1="20.32" y1="17.78" x2="12.7" y2="17.78" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PA07"/>
<wire x1="20.32" y1="20.32" x2="22.86" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<wire x1="17.78" y1="25.4" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<pinref part="R4" gate="G$1" pin="1"/>
<wire x1="12.7" y1="12.7" x2="17.78" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="PA05"/>
<wire x1="22.86" y1="25.4" x2="17.78" y2="25.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="CGREEN"/>
<wire x1="2.54" y1="22.86" x2="0" y2="22.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="D1" gate="G$1" pin="CBLUE"/>
<pinref part="R3" gate="G$1" pin="2"/>
<wire x1="0" y1="17.78" x2="2.54" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R4" gate="G$1" pin="2"/>
<pinref part="D1" gate="G$1" pin="CRED"/>
<wire x1="2.54" y1="12.7" x2="0" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SWDCLK" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA30"/>
<wire x1="58.42" y1="10.16" x2="63.5" y2="10.16" width="0.1524" layer="91"/>
<label x="58.42" y="10.16" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="60.96" y1="83.82" x2="71.12" y2="83.82" width="0.1524" layer="91"/>
<label x="60.96" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-4" pin="S"/>
<wire x1="48.26" y1="142.24" x2="66.04" y2="142.24" width="0.1524" layer="91"/>
<label x="60.96" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="SWDIO" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA31"/>
<wire x1="58.42" y1="7.62" x2="63.5" y2="7.62" width="0.1524" layer="91"/>
<label x="58.42" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="60.96" y1="86.36" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<label x="60.96" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-2" pin="S"/>
<wire x1="48.26" y1="144.78" x2="66.04" y2="144.78" width="0.1524" layer="91"/>
<label x="60.96" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="AOUT1"/>
<wire x1="96.52" y1="38.1" x2="81.28" y2="38.1" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="2"/>
<wire x1="81.28" y1="38.1" x2="81.28" y2="35.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="U$3" gate="G$1" pin="4"/>
<pinref part="U$1" gate="G$1" pin="AOUT2"/>
<wire x1="83.82" y1="35.56" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<wire x1="96.52" y1="35.56" x2="96.52" y2="33.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="BOUT2"/>
<pinref part="U$3" gate="G$1" pin="3"/>
<wire x1="96.52" y1="27.94" x2="83.82" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="BOUT1"/>
<wire x1="96.52" y1="22.86" x2="81.28" y2="22.86" width="0.1524" layer="91"/>
<pinref part="U$3" gate="G$1" pin="1"/>
<wire x1="81.28" y1="22.86" x2="81.28" y2="27.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DAC" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA02"/>
<wire x1="22.86" y1="33.02" x2="15.24" y2="33.02" width="0.1524" layer="91"/>
<label x="15.24" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VREF"/>
<wire x1="132.08" y1="12.7" x2="132.08" y2="5.08" width="0.1524" layer="91"/>
<label x="132.08" y="7.62" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R5" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="60.96" x2="-2.54" y2="60.96" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="60.96" x2="-2.54" y2="71.12" width="0.1524" layer="91"/>
<pinref part="R5" gate="G$1" pin="2"/>
<wire x1="-2.54" y1="71.12" x2="-7.62" y2="71.12" width="0.1524" layer="91"/>
<label x="-5.08" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="VM" class="0">
<segment>
<pinref part="U$4" gate="G$1" pin="2"/>
<wire x1="91.44" y1="73.66" x2="104.14" y2="73.66" width="0.1524" layer="91"/>
<label x="99.06" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$1" gate="G$1" pin="VM"/>
<wire x1="96.52" y1="48.26" x2="93.98" y2="48.26" width="0.1524" layer="91"/>
<wire x1="93.98" y1="48.26" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<label x="93.98" y="53.34" size="1.778" layer="95"/>
<pinref part="BIGBOYCAP" gate="&gt;NAME" pin="2"/>
<pinref part="C2" gate="&gt;NAME" pin="2"/>
<wire x1="93.98" y1="60.96" x2="93.98" y2="55.88" width="0.1524" layer="91"/>
<junction x="93.98" y="55.88"/>
</segment>
</net>
<net name="DIR" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DIR"/>
<wire x1="132.08" y1="38.1" x2="137.16" y2="38.1" width="0.1524" layer="91"/>
<label x="137.16" y="38.1" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA00"/>
<wire x1="22.86" y1="38.1" x2="15.24" y2="38.1" width="0.1524" layer="91"/>
<label x="17.78" y="38.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="STEP" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="STEP"/>
<wire x1="132.08" y1="33.02" x2="137.16" y2="33.02" width="0.1524" layer="91"/>
<label x="134.62" y="33.02" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA01"/>
<wire x1="22.86" y1="35.56" x2="15.24" y2="35.56" width="0.1524" layer="91"/>
<label x="17.78" y="35.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="M1" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="M1"/>
<wire x1="132.08" y1="27.94" x2="137.16" y2="27.94" width="0.1524" layer="91"/>
<label x="134.62" y="27.94" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA03"/>
<wire x1="22.86" y1="30.48" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
<label x="17.78" y="30.48" size="1.778" layer="95"/>
</segment>
</net>
<net name="M0" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="M0"/>
<wire x1="132.08" y1="17.78" x2="137.16" y2="17.78" width="0.1524" layer="91"/>
<label x="134.62" y="17.78" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="PA04"/>
<wire x1="22.86" y1="27.94" x2="15.24" y2="27.94" width="0.1524" layer="91"/>
<label x="17.78" y="27.94" size="1.778" layer="95"/>
</segment>
</net>
<net name="RESET" class="0">
<segment>
<wire x1="60.96" y1="76.2" x2="71.12" y2="76.2" width="0.1524" layer="91"/>
<label x="63.5" y="76.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U1" gate="G$1" pin="!RESET"/>
<pinref part="S1" gate="2" pin="S1"/>
<wire x1="22.86" y1="43.18" x2="15.24" y2="43.18" width="0.1524" layer="91"/>
<pinref part="S1" gate="2" pin="S"/>
<wire x1="15.24" y1="43.18" x2="12.7" y2="43.18" width="0.1524" layer="91"/>
<wire x1="12.7" y1="43.18" x2="10.16" y2="43.18" width="0.1524" layer="91"/>
<junction x="12.7" y="43.18"/>
<pinref part="R1" gate="G$1" pin="1"/>
<wire x1="15.24" y1="45.72" x2="15.24" y2="43.18" width="0.1524" layer="91"/>
<junction x="15.24" y="43.18"/>
<label x="15.24" y="43.18" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="R6" gate="G$1" pin="1"/>
<wire x1="-7.62" y1="76.2" x2="-5.08" y2="76.2" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="76.2" x2="-5.08" y2="86.36" width="0.1524" layer="91"/>
<pinref part="R6" gate="G$1" pin="2"/>
<wire x1="-5.08" y1="86.36" x2="-7.62" y2="86.36" width="0.1524" layer="91"/>
<label x="-5.08" y="86.36" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="-10" pin="S"/>
<wire x1="48.26" y1="134.62" x2="66.04" y2="134.62" width="0.1524" layer="91"/>
<label x="60.96" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="VDD" class="0">
<segment>
<pinref part="U$1" gate="G$1" pin="DVDD"/>
<wire x1="96.52" y1="12.7" x2="88.9" y2="12.7" width="0.1524" layer="91"/>
<label x="88.9" y="12.7" size="1.778" layer="95"/>
<pinref part="C3" gate="&gt;NAME" pin="2"/>
<wire x1="88.9" y1="10.16" x2="88.9" y2="12.7" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
