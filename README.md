## What is the goal of this:  
Series of demos for mechanical and electrical concepts to catalize learning process for components that need to be experienced.  Double sided displays with mechanics on front and electronics on back.  
Motors- types and functions  
Motion- methods components examples  (Gears linkages and what not)  
Electronics- quick blurb about boards controlling things because why not.  Including some design decisions.      
Machine based- could do 3d printing laser cutting etc boards to show what each machine is good at and what shouldn't be done.  



## Breakdown:  
  
### Motors  
DC Servo(analog and digital) Stepper BLDC advantages/disadvantages of each.  Methods of controlling.  Speed control torque control knobs and pots for user inputs.  Measuring speed with encoders (magnet, optical, reflector hack based quadrature and basic)  Maybe more than one size of each.  Torque speccing.  Driving with H bridges (great segway into H bridge circuit example) vs PWM signals vs FETs. Microstepping.  Servo dissection to show simplicity.  Open vs Closed loop  
  
### Motion  
Gears (spur bevel involute rack and pinion) linkages bearings Cams? belts/pulleys (timing and V) 3d printed wheels with TPU tire for grip? 3d printed bearings and real bearings. Hinges (living and rotational)   Stateful mechanisms (push latches etc)

### Electronics  
.brd and .sch of all the boards involved short breakdown of the design decisions for each of them.  Boards mostly based on tiny 1 and SAMD series.  Might throw in ESP demo for S&Gs.  Could do demo on wireless communication methods (wifi bluetooth/BLE Zigbee LORA etc).  Could also do a cool demo on voltage conversion ie efficiency of resistive dividers ldo buck/boost

### Code  
Not emphesized as most should be simple examples.  .ino files stored in respective folders in case of loss of firmware


## Overarching ideals:  
Essentially a series of poster boards (birch ply?) with an emphesis on user input and interactability.  Knobs that control speeds/torques/direction/position LED displays reading of speed and what not maybe current/voltage readings.  switches to enable/disable things.  Coatings on boards so they don't tarnish.  trying to pack everything on to panels so they don't need a companion webpage/gui for visualization but may not be possible for some.  gitlab should be used for things that need more explanation.  Lots of things to touch/feel.  Nothing hidden so wires and everything routed neatly.  Probably some MIT EECS logo/border/theme to tie everything together w/ vinyl cut decal.  Large format printer from copytech would look better and be easier   


