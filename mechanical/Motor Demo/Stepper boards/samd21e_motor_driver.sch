<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.5.2">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="12" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="tMap" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="tPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="bPlate" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="Crop" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="tplace-old" color="10" fill="1" visible="no" active="yes"/>
<layer number="109" name="ref-old" color="11" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="IDFDebug" color="7" fill="1" visible="no" active="yes"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="no" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="no" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="no" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="yes"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="no" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="yes"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="yes"/>
<layer number="155" name="FabDoc3" color="7" fill="1" visible="no" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="10" visible="no" active="yes"/>
<layer number="202" name="202bmp" color="3" fill="10" visible="no" active="yes"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="231" name="231bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="yes"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="no" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ATSAMD21E18A-AUT">
<packages>
<package name="QFP80P900X900X120-32N">
<text x="-5.225" y="-5.33" size="1.27" layer="27" align="top-left">&gt;VALUE</text>
<text x="-5.225" y="5.43" size="1.27" layer="25">&gt;NAME</text>
<circle x="-5.98" y="2.8" radius="0.1" width="0.2" layer="21"/>
<circle x="-5.98" y="2.8" radius="0.1" width="0.2" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="-3.5" y2="-3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="3.5" x2="-3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="3.5" width="0.127" layer="51"/>
<wire x1="3.5" y1="-3.5" x2="3.395" y2="-3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.395" y2="3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.395" y2="-3.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.395" y2="3.5" width="0.127" layer="21"/>
<wire x1="3.5" y1="-3.5" x2="3.5" y2="-3.395" width="0.127" layer="21"/>
<wire x1="3.5" y1="3.5" x2="3.5" y2="3.395" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-3.5" x2="-3.5" y2="-3.395" width="0.127" layer="21"/>
<wire x1="-3.5" y1="3.5" x2="-3.5" y2="3.395" width="0.127" layer="21"/>
<wire x1="-5.23" y1="-5.23" x2="5.23" y2="-5.23" width="0.05" layer="39"/>
<wire x1="-5.23" y1="5.23" x2="5.23" y2="5.23" width="0.05" layer="39"/>
<wire x1="-5.23" y1="-5.23" x2="-5.23" y2="5.23" width="0.05" layer="39"/>
<wire x1="5.23" y1="-5.23" x2="5.23" y2="5.23" width="0.05" layer="39"/>
<smd name="9" x="-2.8" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="10" x="-2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="11" x="-1.2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="12" x="-0.4" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="13" x="0.4" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="14" x="1.2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="15" x="2" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="16" x="2.8" y="-4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="25" x="2.8" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="26" x="2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="27" x="1.2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="28" x="0.4" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="29" x="-0.4" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="30" x="-1.2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="31" x="-2" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="32" x="-2.8" y="4.18" dx="1.6" dy="0.55" layer="1" roundness="25" rot="R90"/>
<smd name="1" x="-4.18" y="2.8" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="2" x="-4.18" y="2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="3" x="-4.18" y="1.2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="4" x="-4.18" y="0.4" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="5" x="-4.18" y="-0.4" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="6" x="-4.18" y="-1.2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="7" x="-4.18" y="-2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="8" x="-4.18" y="-2.8" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="17" x="4.18" y="-2.8" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="18" x="4.18" y="-2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="19" x="4.18" y="-1.2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="20" x="4.18" y="-0.4" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="21" x="4.18" y="0.4" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="22" x="4.18" y="1.2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="23" x="4.18" y="2" dx="1.6" dy="0.55" layer="1" roundness="25"/>
<smd name="24" x="4.18" y="2.8" dx="1.6" dy="0.55" layer="1" roundness="25"/>
</package>
</packages>
<symbols>
<symbol name="ATSAMD21E18A-AUT">
<wire x1="-12.7" y1="25.4" x2="12.7" y2="25.4" width="0.254" layer="94"/>
<wire x1="12.7" y1="25.4" x2="12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="-12.7" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-12.7" y1="-27.94" x2="-12.7" y2="25.4" width="0.254" layer="94"/>
<text x="-12.7" y="26.67" size="1.778" layer="95">&gt;NAME</text>
<text x="-12.7" y="-30.48" size="1.778" layer="96">&gt;VALUE</text>
<pin name="PA00" x="-17.78" y="10.16" length="middle"/>
<pin name="PA01" x="-17.78" y="7.62" length="middle"/>
<pin name="PA02" x="-17.78" y="5.08" length="middle"/>
<pin name="PA03" x="-17.78" y="2.54" length="middle"/>
<pin name="PA04" x="-17.78" y="0" length="middle"/>
<pin name="PA05" x="-17.78" y="-2.54" length="middle"/>
<pin name="PA06" x="-17.78" y="-5.08" length="middle"/>
<pin name="PA07" x="-17.78" y="-7.62" length="middle"/>
<pin name="PA08" x="-17.78" y="-10.16" length="middle"/>
<pin name="PA09" x="-17.78" y="-12.7" length="middle"/>
<pin name="PA10" x="-17.78" y="-15.24" length="middle"/>
<pin name="PA11" x="-17.78" y="-17.78" length="middle"/>
<pin name="PA14" x="-17.78" y="-20.32" length="middle"/>
<pin name="PA15" x="17.78" y="10.16" length="middle" rot="R180"/>
<pin name="PA16" x="17.78" y="7.62" length="middle" rot="R180"/>
<pin name="PA17" x="17.78" y="5.08" length="middle" rot="R180"/>
<pin name="PA18" x="17.78" y="2.54" length="middle" rot="R180"/>
<pin name="PA19" x="17.78" y="0" length="middle" rot="R180"/>
<pin name="PA22" x="17.78" y="-2.54" length="middle" rot="R180"/>
<pin name="PA23" x="17.78" y="-5.08" length="middle" rot="R180"/>
<pin name="PA24" x="17.78" y="-7.62" length="middle" rot="R180"/>
<pin name="PA25" x="17.78" y="-10.16" length="middle" rot="R180"/>
<pin name="PA27" x="17.78" y="-12.7" length="middle" rot="R180"/>
<pin name="PA28" x="17.78" y="-15.24" length="middle" rot="R180"/>
<pin name="PA30" x="17.78" y="-17.78" length="middle" rot="R180"/>
<pin name="PA31" x="17.78" y="-20.32" length="middle" rot="R180"/>
<pin name="!RESET" x="-17.78" y="15.24" length="middle" direction="in"/>
<pin name="VDDANA" x="17.78" y="17.78" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDIN" x="17.78" y="20.32" length="middle" direction="pwr" rot="R180"/>
<pin name="VDDCORE" x="17.78" y="22.86" length="middle" direction="pwr" rot="R180"/>
<pin name="GND" x="17.78" y="-25.4" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="ATSAMD21E18A-AUT" prefix="U">
<description>ARM Cortex-M0+ SAM D21E Microcontroller IC 32-Bit 48MHz 256KB (256K x 8) FLASH 32-TQFP (7x7)   &lt;a href="https://pricing.snapeda.com/parts/ATSAMD21E18A-AU/Microchip%20Technology/view-part?ref=eda"&gt;Check prices&lt;/a&gt;</description>
<gates>
<gate name="G$1" symbol="ATSAMD21E18A-AUT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="QFP80P900X900X120-32N">
<connects>
<connect gate="G$1" pin="!RESET" pad="26"/>
<connect gate="G$1" pin="GND" pad="10 28"/>
<connect gate="G$1" pin="PA00" pad="1"/>
<connect gate="G$1" pin="PA01" pad="2"/>
<connect gate="G$1" pin="PA02" pad="3"/>
<connect gate="G$1" pin="PA03" pad="4"/>
<connect gate="G$1" pin="PA04" pad="5"/>
<connect gate="G$1" pin="PA05" pad="6"/>
<connect gate="G$1" pin="PA06" pad="7"/>
<connect gate="G$1" pin="PA07" pad="8"/>
<connect gate="G$1" pin="PA08" pad="11"/>
<connect gate="G$1" pin="PA09" pad="12"/>
<connect gate="G$1" pin="PA10" pad="13"/>
<connect gate="G$1" pin="PA11" pad="14"/>
<connect gate="G$1" pin="PA14" pad="15"/>
<connect gate="G$1" pin="PA15" pad="16"/>
<connect gate="G$1" pin="PA16" pad="17"/>
<connect gate="G$1" pin="PA17" pad="18"/>
<connect gate="G$1" pin="PA18" pad="19"/>
<connect gate="G$1" pin="PA19" pad="20"/>
<connect gate="G$1" pin="PA22" pad="21"/>
<connect gate="G$1" pin="PA23" pad="22"/>
<connect gate="G$1" pin="PA24" pad="23"/>
<connect gate="G$1" pin="PA25" pad="24"/>
<connect gate="G$1" pin="PA27" pad="25"/>
<connect gate="G$1" pin="PA28" pad="27"/>
<connect gate="G$1" pin="PA30" pad="31"/>
<connect gate="G$1" pin="PA31" pad="32"/>
<connect gate="G$1" pin="VDDANA" pad="9"/>
<connect gate="G$1" pin="VDDCORE" pad="29"/>
<connect gate="G$1" pin="VDDIN" pad="30"/>
</connects>
<technologies>
<technology name="">
<attribute name="AVAILABILITY" value="Good"/>
<attribute name="DESCRIPTION" value=" IC MCU 32BIT 256KB FLASH 32TQFP "/>
<attribute name="MF" value="Microchip Technology"/>
<attribute name="MP" value="ATSAMD21E18A-AU"/>
<attribute name="PACKAGE" value="None"/>
<attribute name="PRICE" value="None"/>
<attribute name="PURCHASE-URL" value="https://pricing.snapeda.com/search/part/ATSAMD21E18A-AU/?ref=eda"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1" urn="urn:adsk.eagle:library:371">
<description>&lt;b&gt;Supply Symbols&lt;/b&gt;&lt;p&gt;
 GND, VCC, 0V, +5V, -5V, etc.&lt;p&gt;
 Please keep in mind, that these devices are necessary for the
 automatic wiring of the supply signals.&lt;p&gt;
 The pin name defined in the symbol is identical to the net which is to be wired automatically.&lt;p&gt;
 In this library the device names are the same as the pin names of the symbols, therefore the correct signal names appear next to the supply symbols in the schematic.&lt;p&gt;
 &lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
</packages>
<symbols>
<symbol name="GND" urn="urn:adsk.eagle:symbol:26925/1" library_version="1">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND" urn="urn:adsk.eagle:component:26954/1" prefix="GND" library_version="1">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="fab">
<packages>
<package name="DX4R005HJ5_100">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.35" dy="1.35" layer="1"/>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
</package>
<package name="DX4R005HJ5">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.4" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="25" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="DX4R005HJ5_64">
<wire x1="3.25" y1="-2.6" x2="-3.25" y2="-2.6" width="0.127" layer="21"/>
<wire x1="-3.25" y1="2.6" x2="-3.25" y2="0" width="0.127" layer="51"/>
<wire x1="3.25" y1="2.6" x2="3.25" y2="0" width="0.127" layer="51"/>
<wire x1="-1.75" y1="2.6" x2="1.75" y2="2.6" width="0.127" layer="51"/>
<wire x1="-3.25" y1="-2.2" x2="-3.25" y2="-2.6" width="0.127" layer="51"/>
<wire x1="3.25" y1="-2.6" x2="3.25" y2="-2.2" width="0.127" layer="51"/>
<smd name="GND@3" x="-2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@4" x="2.175" y="-1.1" dx="2.15" dy="1.9" layer="1"/>
<smd name="GND@1" x="-2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="GND@2" x="2.5" y="1.95" dx="1.2" dy="1.3" layer="1"/>
<smd name="D+" x="0" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="D-" x="-0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="VBUS" x="-1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="ID" x="0.65" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<smd name="GND" x="1.3" y="1.6" dx="0.254" dy="1.35" layer="1"/>
<text x="-3.4925" y="-1.27" size="0.6096" layer="25" font="vector" rot="R90">&gt;Name</text>
<text x="4.1275" y="-1.5875" size="0.6096" layer="27" font="vector" rot="R90">&gt;Value</text>
</package>
<package name="JACK_2.1MM">
<wire x1="-4.445" y1="-7.493" x2="4.5466" y2="-7.493" width="0.127" layer="21"/>
<wire x1="-4.445" y1="-7.493" x2="-4.445" y2="7.3152" width="0.127" layer="21"/>
<wire x1="-4.445" y1="7.3152" x2="4.5466" y2="7.3152" width="0.127" layer="21"/>
<wire x1="4.5466" y1="-7.493" x2="4.5466" y2="7.3152" width="0.127" layer="21"/>
<wire x1="0" y1="-7.366" x2="0" y2="-2.54" width="0.6096" layer="21"/>
<wire x1="0" y1="-2.54" x2="0" y2="3.81" width="0.6096" layer="21"/>
<wire x1="0" y1="3.81" x2="0" y2="4.826" width="0.6096" layer="21"/>
<smd name="PIN" x="-5.6388" y="-2.413" dx="2.3876" dy="2.794" layer="1"/>
<smd name="PIN1" x="-5.6388" y="3.683" dx="2.3876" dy="2.794" layer="1"/>
<smd name="CONTACT" x="5.7658" y="-2.413" dx="2.3876" dy="2.794" layer="1"/>
<smd name="SHUNT" x="5.7658" y="3.683" dx="2.3876" dy="2.794" layer="1"/>
</package>
<package name="JACK_.65MM">
<wire x1="-2.4226" y1="6.0396" x2="2.5562" y2="6.0396" width="0.127" layer="21"/>
<wire x1="2.5562" y1="6.0396" x2="2.5562" y2="-4.9705" width="0.127" layer="21"/>
<wire x1="2.5562" y1="-4.9705" x2="-2.4226" y2="-4.9705" width="0.127" layer="21"/>
<wire x1="-2.4226" y1="-4.9705" x2="-2.4226" y2="6.0395" width="0.127" layer="21"/>
<wire x1="0.0339" y1="-4.9921" x2="0.0339" y2="3.3549" width="0.6096" layer="21"/>
<smd name="SHUNT" x="-3.4504" y="5.0052" dx="2.032" dy="2.54" layer="1"/>
<smd name="SHIELD" x="-3.4504" y="-1.4484" dx="2.032" dy="2.54" layer="1"/>
<smd name="SHIELD1" x="3.5782" y="-0.4473" dx="2.032" dy="2.54" layer="1"/>
<smd name="CONTACT" x="3.5781" y="4.0255" dx="2.032" dy="2.54" layer="1"/>
<smd name="PIN" x="0.0639" y="7.0712" dx="1.778" dy="2.032" layer="1"/>
</package>
<package name="SOD123">
<description>&lt;b&gt;SMALL OUTLINE DIODE&lt;/b&gt;</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.321" y1="0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="1.321" y2="-0.787" width="0.1016" layer="51"/>
<wire x1="-1.321" y1="-0.787" x2="-1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="1.321" y1="-0.787" x2="1.321" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-1" y1="0" x2="0" y2="0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="0.5" x2="0" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="0" y1="-0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="0" width="0.2032" layer="51"/>
<wire x1="-1" y1="0" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="CATHODE" x="-1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<smd name="ANODE" x="1.7" y="0" dx="1.6" dy="0.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.9558" y1="-0.3048" x2="-1.3716" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="1.3716" y1="-0.3048" x2="1.9558" y2="0.3048" layer="51" rot="R180"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-0.889" x2="2.794" y2="-0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="-0.889" x2="2.794" y2="0.889" width="0.127" layer="21"/>
<wire x1="2.794" y1="0.889" x2="-2.921" y2="0.889" width="0.127" layer="21"/>
</package>
<package name="SOT23">
<description>&lt;b&gt;SOT 23&lt;/b&gt;</description>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="1.4224" y1="-0.6604" x2="-1.4224" y2="-0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.6604" x2="-1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="51"/>
<wire x1="-1.4224" y1="-0.1524" x2="-1.4224" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="-1.4224" y1="0.6604" x2="-0.8636" y2="0.6604" width="0.1524" layer="21"/>
<wire x1="1.4224" y1="0.6604" x2="1.4224" y2="-0.1524" width="0.1524" layer="21"/>
<wire x1="0.8636" y1="0.6604" x2="1.4224" y2="0.6604" width="0.1524" layer="21"/>
<smd name="3" x="0" y="1.1" dx="1" dy="1.4" layer="1"/>
<smd name="2" x="0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<smd name="1" x="-0.95" y="-1.1" dx="1" dy="1.4" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2286" y1="0.7112" x2="0.2286" y2="1.2954" layer="51"/>
<rectangle x1="0.7112" y1="-1.2954" x2="1.1684" y2="-0.7112" layer="51"/>
<rectangle x1="-1.1684" y1="-1.2954" x2="-0.7112" y2="-0.7112" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="USB-1">
<wire x1="6.35" y1="-2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="-3.81" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<text x="-2.54" y="-1.27" size="2.54" layer="94">USB</text>
<text x="-4.445" y="-1.905" size="1.27" layer="95" font="vector" rot="R90">&gt;Name</text>
<text x="8.255" y="-1.905" size="1.27" layer="96" font="vector" rot="R90">&gt;Value</text>
<pin name="D+" x="5.08" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="D-" x="2.54" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="VBUS" x="0" y="5.08" visible="pad" length="short" rot="R270"/>
<pin name="GND" x="-2.54" y="5.08" visible="pad" length="short" rot="R270"/>
</symbol>
<symbol name="JACK">
<wire x1="-2.54" y1="3.81" x2="-2.54" y2="2.54" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-3.81" x2="0" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="3.81" x2="-2.54" y2="3.81" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="0" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.905" y2="-0.635" width="0.254" layer="94"/>
<text x="-2.54" y="4.9514" size="1.9304" layer="95">&gt;NAME</text>
<pin name="PIN" x="-5.08" y="2.54" visible="off" length="short"/>
<pin name="CONTACT" x="5.08" y="-2.54" visible="off" length="short" rot="R180"/>
<pin name="SHUNT" x="5.08" y="2.54" visible="off" length="short" rot="R180"/>
</symbol>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.3114" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="short" direction="pas"/>
<pin name="C" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="REGULATOR">
<wire x1="-6.35" y1="5.08" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="0" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="0" y1="-1.27" x2="6.35" y2="-1.27" width="0.4064" layer="94"/>
<wire x1="6.35" y1="-1.27" x2="6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="6.35" y1="2.54" x2="6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="6.35" y1="5.08" x2="-6.35" y2="5.08" width="0.4064" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="-6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<text x="-6.35" y="-3.81" size="1.27" layer="95">&gt;NAME</text>
<text x="1.27" y="-3.81" size="1.27" layer="96">&gt;VALUE</text>
<pin name="IN" x="-7.62" y="2.54" length="point"/>
<pin name="GND" x="0" y="-2.54" length="point" rot="R90"/>
<pin name="OUT" x="7.62" y="2.54" length="point" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CONN_MICRO-USB" prefix="X">
<description>SMD micro USB connector as found in the fablab inventory. 
Three footprint variants included: 
&lt;ol&gt;
&lt;li&gt; original, as described by manufacturer's datasheet
&lt;li&gt; for milling with the 1/100" bit
&lt;li&gt; for milling with the 1/64" bit
&lt;/ol&gt;
&lt;p&gt;Made by Zaerc.</description>
<gates>
<gate name="G$1" symbol="USB-1" x="0" y="0"/>
</gates>
<devices>
<device name="_1/100" package="DX4R005HJ5_100">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_ORIG" package="DX4R005HJ5">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="_1/64" package="DX4R005HJ5_64">
<connects>
<connect gate="G$1" pin="D+" pad="D+"/>
<connect gate="G$1" pin="D-" pad="D-"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="VBUS" pad="VBUS"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CONN_JACK" prefix="J">
<gates>
<gate name="G$1" symbol="JACK" x="0" y="0"/>
</gates>
<devices>
<device name="2.1MM" package="JACK_2.1MM">
<connects>
<connect gate="G$1" pin="CONTACT" pad="CONTACT"/>
<connect gate="G$1" pin="PIN" pad="PIN"/>
<connect gate="G$1" pin="SHUNT" pad="SHUNT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name=".65MM" package="JACK_.65MM">
<connects>
<connect gate="G$1" pin="CONTACT" pad="SHUNT"/>
<connect gate="G$1" pin="PIN" pad="PIN"/>
<connect gate="G$1" pin="SHUNT" pad="CONTACT"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DIODE_SOD-123" prefix="D" uservalue="yes">
<description>&lt;B&gt;DIODE&lt;/B&gt;&lt;p&gt;
high speed (Philips)</description>
<gates>
<gate name="G$1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="SOD123" package="SOD123">
<connects>
<connect gate="G$1" pin="A" pad="ANODE"/>
<connect gate="G$1" pin="C" pad="CATHODE"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VR_REGULATOR-SOT23" prefix="IC" uservalue="yes">
<gates>
<gate name="G$1" symbol="REGULATOR" x="0" y="0"/>
</gates>
<devices>
<device name="SOT23" package="SOT23">
<connects>
<connect gate="G$1" pin="GND" pad="3"/>
<connect gate="G$1" pin="IN" pad="2"/>
<connect gate="G$1" pin="OUT" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U1" library="ATSAMD21E18A-AUT" deviceset="ATSAMD21E18A-AUT" device=""/>
<part name="GND1" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="X1" library="fab" deviceset="CONN_MICRO-USB" device="_1/64"/>
<part name="J2" library="fab" deviceset="CONN_JACK" device="2.1MM"/>
<part name="GND2" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="D1" library="fab" deviceset="DIODE_SOD-123" device="SOD123"/>
<part name="GND3" library="supply1" library_urn="urn:adsk.eagle:library:371" deviceset="GND" device=""/>
<part name="IC1" library="fab" deviceset="VR_REGULATOR-SOT23" device="SOT23"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="U1" gate="G$1" x="43.18" y="55.88" smashed="yes">
<attribute name="NAME" x="30.48" y="82.55" size="1.778" layer="95"/>
<attribute name="VALUE" x="30.48" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="GND1" gate="1" x="71.12" y="27.94" smashed="yes">
<attribute name="VALUE" x="68.58" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="81.28" y="109.22" smashed="yes" rot="R180">
<attribute name="NAME" x="85.725" y="111.125" size="1.27" layer="95" font="vector" rot="R270"/>
<attribute name="VALUE" x="73.025" y="111.125" size="1.27" layer="96" font="vector" rot="R270"/>
</instance>
<instance part="J2" gate="G$1" x="40.64" y="99.06" smashed="yes">
<attribute name="NAME" x="38.1" y="104.0114" size="1.9304" layer="95"/>
</instance>
<instance part="GND2" gate="1" x="45.72" y="93.98" smashed="yes">
<attribute name="VALUE" x="43.18" y="91.44" size="1.778" layer="96"/>
</instance>
<instance part="D1" gate="G$1" x="78.74" y="91.44" smashed="yes" rot="R180">
<attribute name="NAME" x="76.2" y="90.9574" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="76.2" y="93.7514" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND3" gate="1" x="93.98" y="101.6" smashed="yes">
<attribute name="VALUE" x="91.44" y="99.06" size="1.778" layer="96"/>
</instance>
<instance part="IC1" gate="G$1" x="60.96" y="91.44" smashed="yes" rot="R270">
<attribute name="NAME" x="57.15" y="97.79" size="1.27" layer="95" rot="R270"/>
<attribute name="VALUE" x="57.15" y="90.17" size="1.27" layer="96" rot="R270"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="GND"/>
<wire x1="60.96" y1="30.48" x2="71.12" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND1" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J2" gate="G$1" pin="SHUNT"/>
<pinref part="J2" gate="G$1" pin="CONTACT"/>
<wire x1="45.72" y1="101.6" x2="45.72" y2="96.52" width="0.1524" layer="91"/>
<pinref part="GND2" gate="1" pin="GND"/>
<junction x="45.72" y="96.52"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="58.42" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="50.8" y1="91.44" x2="50.8" y2="96.52" width="0.1524" layer="91"/>
<wire x1="50.8" y1="96.52" x2="45.72" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="GND"/>
<wire x1="83.82" y1="104.14" x2="93.98" y2="104.14" width="0.1524" layer="91"/>
<pinref part="GND3" gate="1" pin="GND"/>
</segment>
</net>
<net name="9V" class="0">
<segment>
<pinref part="J2" gate="G$1" pin="PIN"/>
<wire x1="35.56" y1="101.6" x2="33.02" y2="101.6" width="0.1524" layer="91"/>
<label x="30.48" y="101.6" size="1.778" layer="95"/>
<wire x1="33.02" y1="101.6" x2="30.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="33.02" y1="101.6" x2="33.02" y2="109.22" width="0.1524" layer="91"/>
<junction x="33.02" y="101.6"/>
<pinref part="D1" gate="G$1" pin="C"/>
<wire x1="76.2" y1="91.44" x2="71.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="71.12" y1="91.44" x2="71.12" y2="99.06" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="IN"/>
<wire x1="71.12" y1="99.06" x2="63.5" y2="99.06" width="0.1524" layer="91"/>
<wire x1="33.02" y1="109.22" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<wire x1="63.5" y1="109.22" x2="63.5" y2="99.06" width="0.1524" layer="91"/>
<junction x="63.5" y="99.06"/>
</segment>
</net>
<net name="3V3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="VDDANA"/>
<pinref part="U1" gate="G$1" pin="VDDIN"/>
<wire x1="60.96" y1="73.66" x2="60.96" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="VDDCORE"/>
<wire x1="60.96" y1="76.2" x2="60.96" y2="78.74" width="0.1524" layer="91"/>
<junction x="60.96" y="76.2"/>
<wire x1="60.96" y1="78.74" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="60.96" y="78.74"/>
<label x="63.5" y="78.74" size="1.778" layer="95"/>
<pinref part="IC1" gate="G$1" pin="OUT"/>
<wire x1="63.5" y1="78.74" x2="71.12" y2="78.74" width="0.1524" layer="91"/>
<wire x1="63.5" y1="83.82" x2="63.5" y2="78.74" width="0.1524" layer="91"/>
<junction x="63.5" y="78.74"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="X1" gate="G$1" pin="VBUS"/>
<pinref part="D1" gate="G$1" pin="A"/>
<wire x1="81.28" y1="104.14" x2="81.28" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="DM" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA24"/>
<wire x1="60.96" y1="48.26" x2="66.04" y2="48.26" width="0.1524" layer="91"/>
<label x="63.5" y="48.26" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="D-"/>
<wire x1="78.74" y1="104.14" x2="78.74" y2="99.06" width="0.1524" layer="91"/>
<label x="78.74" y="99.06" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="DP" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="PA25"/>
<wire x1="60.96" y1="45.72" x2="66.04" y2="45.72" width="0.1524" layer="91"/>
<label x="63.5" y="45.72" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="X1" gate="G$1" pin="D+"/>
<wire x1="76.2" y1="104.14" x2="76.2" y2="99.06" width="0.1524" layer="91"/>
<label x="76.2" y="99.06" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
</compatibility>
</eagle>
