01/21/21
setup gitlab  
started global framing

---

01/22/21
Where possible boards will be based on SAMD21E (used in arduino zero)"SAMD21E15AAU"  has 32 pins with 26 gpio tqfp package 48 MHz.  Needs a bootloader for USB programming probably comes from JTAG then a micro/mini usb for serial and programming afterwards.  3.3V   
include ptc fuse? Can't hurt  
debug port?  probably not needed  
barrel jacks for power.  fet/diode protection for situation with jack and usb plugged in together  
standardized mounting scheme with standoffs  
tx/rx lights  
pin labels printed on poster around where board is mounted  
LED on D13 to check bootloader  (pulses when in bootloader)  
Reset buttons  
Single board to cover all motors?  Seems like pin count might be an issue.  Single board for each motor type sounds better. Still close if too many steppers.  Might ditch unipolar or upgrade to samd21G for this guy.  

some board progress but lots of open questions to answer first
emailed tasker to see if docs on pappalardo demos exist as a stepping stone
it seems like there will be too much information to fit on a single board.  best bet could be companion .md for each board which would lend well to lecture script
  
**Stepper board:**  
Use A4950 for direct HTMA comparisson  
say nema 14 and 23 bipolar single unipolar example  (~12 GPIO pins)  
knob for speed and switch for direction of each (3 Analog pins 3 digital pins)  
- Can go super slow to see each individual step and also too fast so steps are skipped to show weakness of open loop  

On/Off switch? probably not needed but could use dpdt switch to control VDD to each coil so 1 switch 1 motor and no GPIO needed  
Setting for microstepping probably a toggle switch  
display rpm?  Nifty if possible but not needed.  Can do directly from software given known step rate.  Don't need encoder.  
Button to cycle from constant speed vs goal position?  
if above then indicator lights determine mode?  
in position mode pre canned paths like +/-90 deg rotations.  Don't get too complicated.  Knobs in this mode do nothing? feels like poor design. 
Getting close to pin count but should be ok  
Could do button to cycle which stepper is powered so it is less distracting  
Would be awesome to set up LEDs to show which coil is powered and in what direction so you can see the current path and step order  
Exploded parts of a stepper or just a good diagram  
More informative that the 2 coil approach that doesn't show how direction is achieved  

---  

01/24/21

I'm thinking maybe better to do a single board for each motor.  Explanations of boards will be more concise and directly relevant.  Code will be more streamlined and serve as a better starting point for other projects.  Power routing would get ugly so maybe custom cables are better than weird barrel jacks and jumpers.  Castellated board would be nice but I'd like boards that are made in house.   Screw terminal blocks are fine.  Can always run from lab supply rather than wall warts.  Since pin count will be much lower could go to samd11c/d or keep the 21 in case we want to iterate and add displays/encoders etc.  Not sure if it makes more sense to just make one and go from there or to super analyze to get design foundation right the first time.  

Got some photos from tasker for reference.  Can come by and take a look soon-ish

What is the order of this?  Not sure it matters.  Might just pick them as I go.  

Bootloader with EDBG should work fine with Jake's programmer.  Bootloaders [here](https://github.com/mattairtech/ArduinoCore-samd) Maybe compatible with "burn bootlader button" eliminating need for edbg?  

  --------------------------
  03/08/21
 lots going on short hiatus here.
 Arduino 2.0 launched.  Seems quite good.  embedded debugger might be nice to document here.  other changes not super relevant.
 Struggling with a cohesive direction.  too many open questions about the direction.  I think the route forward is to just start making things.  changing to fit different designs is easy and having a basis to move from might help ground thing.  Always easy to rewire etc.  Everything needs to be broken out to headers etc anyway.

 ---------------------------
 03/16/21
 Maybe got a name? EECS Project lab
 reached out to first htma class for feedback.  haven't gotten any yet but they're excited.  Also pinging jake for his ideas.  real excited.  want to get further along before I start asking stronger voices for their thoughts. Time to buckle down and start demo boards I think.  Will circle back to overarching ideals once there is some concrete progress.
 First up should be motor series.  Control Stepper/DC/Servo/maybe BLDC in that order.  I should talk about how to determine stepper motor timings (ie ring the coils out then step orders could automate with an easy script cycling through the orders then a button to progress.)

-----------------------------
03/24/15 - 03/25/21
Some board progress.  samd21 needed a footprint adjustment to work with 1/64" rather than 1/100" but I think it's good to go now.  Really needed to slow down the 1/32" endmill as nicking the pads would crack them right off since it's default speed was quite high.  Added 1/32" PCB Conservative to the othermill and fresh endmills seems to have fixed it.
Starting to mess around with catsoop.  Seems like a natural choice for hosting content.  served locally for now.  Joe hosts in the cloud Adam hosts locally.  Both seem viable.  
Some annoyances with ground planes on milled boards.  I think the preferred route is to set their isolation to ~32mil (so it is milled with a 1/32") then just add gnd wires for where that doesn't fit.  Might kick myself because the thermal isolation on the ground pads doesn't happen but we'll see if that's an issue moving forward.

-----------------------------
03/29/21
ugh forgot to give enough room for the USB plug head.  Connector needs to be closer to end of board.  Shaved plug to fix on this one.  Also forgot to wire up SWDIO and SWDCLK jumped them and it works.

-----------------------------
03/30/21

So running into some trouble.  
Burning a bootloader with the ice and atmel studio works fine.  It seems the 21e15B doesn't have many bootloaders floating around though.  tried the 15A but it isn't recognized as a device.  doesn't come up in com port.  Poked around and found lots of good resources but not much for this guy.  Currently ordering the e17a (more prog space, better supported) to see if that works.  Might go with the 21G so that it matches an arduino M0 but we'll see.
huge collection of bootloaders from mattairtech but no luck there.
Oof also messed up D+/D- 

-----------------------------
04/13/21
Success! switching to the samd21E17A and using the mattairtech bootloader solved the problem.  Can now successfully upload code via usb. Pin associations found here https://github.com/mattairtech/ArduinoCore-samd/tree/master/variants/MT_D21E_revB seems largely functional

Really should put together a page that serves as programming guide and documentation link

-----------------------------
06/14/21
Moving to SAMD21E18A to sync with fab inventory.  128->256KB memory is the main change. same pinout.
will need new bootloader
New stepper board using the drv8428.  Has up to 1/256 microstepping which will be nice.  many more pins but many can be tied to power or ground permanently or use dip switch/swap resistor locations if uC pin count is an issue. Machining and stuffing board today if possible.  might be missing a few parts.  

Other big changes.  Two similar projects have popped up and will likely be documented in same repo but kept separate:  
- Demos for 6.002 which are likely teensy based or analog. not sure about specifics yet.
- Demos for 6.08 hardware.  these should be super fast dives into hardware.  Used more for showing functionality than for explaining how to use it.  More fun less informative maybe?  Goal is just to make future projects more hardware heavy by introducing students to a large array of devices very quickly.
-----------------------------
06/15/21
Ordered DRV8428E instead of DRV8428 which resulted in a long rabbit hole of why the heck doesn't this work.  
SAMD21E18 works fine. Fried a couple but I think it's due to the above chip discrepancy.
Should find a process for increasing trace robustness on the othermill.  Not sure if it should be slower feeds, shallower depth (final depth not per pass depth), or if there is a trace geometry that would be more stable.  8mil traces tend to shear off during milling.  IMO reducing the depth is probably the way to go.  There's an argument here for doing all the traces with the 1/64" so the cutting force is reduced but I think there is a better answer which doesn't take such a hit on time to cut.  Fresh 1/32" and 0.05mm (down from 0.15) depth of cut seems to do the trick!  Depth of cut probably isn't accurate there as othermill references the bed and the stock thickness but we don't account for the tape thickness which is relatively thick.

If this board works what is next?  Should I just batch out a bunch of board styles or take one piece to completion? leaning towards the former.  I think it nets more utility faster.

-----------------------------

06/22/21
works with the proper DRV8428.  
ran into a funky issue after trying to use "upload using programmer" in arduino with the ice.  Will avoid that in the future.  Jake has a 1.5 layer board using the D21 and some A4950s.  Will grab some neater routing from there once things are confirmed working properly.  It has a nice ground plane and keeping a similar form factor might be nice.

-----------------------------
06/23/21
For reference (stolen from MattairTech)
Pins descriptions for the MattairTech MT-D21E (rev B)

 Arduino	| Silk	| Port	| Alternate Function	| Comments (! means not used with this peripheral assignment)
 --------|-------|-------|-----------------------|-------------------------------------------------------------------------------------------
 0	| A0	| PA00	| Xin32			| Xin32
 1	| A1	| PA01	| Xout32		| Xout32
 2	| A2	| PA02	| DAC0			| !EIC/EXTINT[2] ADC/AIN[0] PTC/Y[0] DAC/VOUT
 3	| A3	| PA03	| REFA			| !EIC/EXTINT[3] REF/ADC/VREFA REF/DAC/VREFA ADC/AIN[1] PTC/Y[1]
 4	| A4	| PA04	| REFB			| EIC/EXTINT[4] REF/ADC/VREFB ADC/AIN[4] AC/AIN[0] PTC/Y[2] !SERCOM0/PAD[0] !TCC0/WO[0]
 5	| A5	| PA05	| DAC1(L21)		| EIC/EXTINT[5] ADC/AIN[5] AC/AIN[1] PTC/Y[3] !SERCOM0/PAD[1] !TCC0/WO[1] DAC1(L21)
 6	| A6	| PA06	| LED			| !EIC/EXTINT[6] ADC/AIN[6] AC/AIN[2] PTC/Y[4] !SERCOM0/PAD[2] !TCC1/WO[0] LED
 7	| A7	| PA07	| Voltage Measurement	| !EIC/EXTINT[7] ADC/AIN[7] AC/AIN[3] PTC/Y[5] !SERCOM0/PAD[3] !TCC1/WO[1]
 8	| A8	| PA08	| SDA1/MISO1		| EIC/NMI ADC/AIN[16] PTC/X[0] !SERCOM0/PAD[0] SERCOM2/PAD[0] TCC0/WO[0] !TCC1/WO[2]
 9	| A9	| PA09	| SCL1/SS1		| EIC/EXTINT[9] ADC/AIN[17] PTC/X[1] !SERCOM0/PAD[1] SERCOM2/PAD[1] TCC0/WO[1] !TCC1/WO[3]
10	| A10	| PA10	| TX1			| !EIC/EXTINT[10] ADC/AIN[18] PTC/X[2] SERCOM0/PAD[2] !SERCOM2/PAD[2] !TCC1/WO[0] TCC0/WO[2]
11	| A11	| PA11	| RX1			| !EIC/EXTINT[11] ADC/AIN[19] PTC/X[3] SERCOM0/PAD[3] !SERCOM2/PAD[3] !TCC1/WO[1] TCC0/WO[3]
12	| ---	| ----	| NOT A PIN		| NOT A PIN
13	| ---	| ----	| NOT A PIN		| NOT A PIN
14	| A14	| PA14	| Xin, TX2/MOSI1	| EIC/EXTINT[14] SERCOM2/PAD[2] TC3/WO[0] !TCC0/WO[4] Xin, HOST_ENABLE
15	| A15	| PA15	| Xout, RX2/SCK1	| !EIC/EXTINT[15] SERCOM2/PAD[3] TC3/WO[1] !TCC0/WO[5] Xout
16	| A16	| PA16	| SDA/TX4 w/pullup	| EIC/EXTINT[0] PTC/X[4] SERCOM1/PAD[0] SERCOM3/PAD[0] TCC2/WO[0] !TCC0/WO[6]
17	| A17	| PA17	| SCL/RX4 w/pullup	| EIC/EXTINT[1] PTC/X[5] SERCOM1/PAD[1] SERCOM3/PAD[1] TCC2/WO[1] !TCC0/WO[7]
18	| A18	| PA18	| MOSI			| EIC/EXTINT[2] PTC/X[6] !SERCOM1/PAD[2] SERCOM3/PAD[2] !TC3/WO[0] !TCC0/WO[2]
19	| A19	| PA19	| SCK			| EIC/EXTINT[3] PTC/X[7] !SERCOM1/PAD[3] SERCOM3/PAD[3] !TC3/WO[1] !TCC0/WO[3]
20	| ---	| ----	| NOT A PIN		| NOT A PIN
21	| ---	| ----	| NOT A PIN		| NOT A PIN
22	| A22	| PA22	| MISO			| EIC/EXTINT[6] PTC/X[10] SERCOM3/PAD[0] TC4/WO[0] !TCC0/WO[4]
23	| A23	| PA23	| SS			| EIC/EXTINT[7] PTC/X[11] SERCOM3/PAD[1] TC4/WO[1] !TCC0/WO[5]
24	| A24-| PA24	| USB_NEGATIVE		| USB/DM TC5/WO[0]
25	| A25+| PA25	| USB_POSITIVE		| USB/DP TC5/WO[1]
26	| ---	| ----	| NOT A PIN		| NOT A PIN
27	| A27	| PA27	| A/CS			| EIC/EXTINT[15] A/CS (Jumper A / memory device chip select)
28	| A28	| PA28	|			| EIC/EXTINT[8]
29	| ---	| ----	| NOT A PIN		| NOT A PIN
30	| A30	| PA30	| SWDCLK / TX3		| EIC/EXTINT[10] SERCOM1/PAD[2] TCC1/WO[0] SWD CLK, leave floating during boot
31	| A31	| PA31	| Button B / SWDIO / RX3| EIC/EXTINT[11] SERCOM1/PAD[3] TCC1/WO[1] Button B SWD IO
--	| RST	| ----	|			| Reset, BOOT (double tap bootloader entry)


* Most pins can be used for more than one function. The port pin number printed
  on the board is also used in Arduino (but without the 'A') for all of the supported
  functions (ie: digitalRead(), analogRead(), analogWrite(), attachInterrupt(), etc.).
* The following Arduino pin numbers are not mapped to a physical pin: 12, 13, 20, 21, 26, and 29.
* Pins 24 and 25 are by default in use by USB (USB_NEGATIVE and USB_POSITIVE).
* TC5(D21) is available on these pins otherwise. The tone library uses TC5.
* A0 and A1 are by default connected to the 32.768KHz crystal.
* Leave pin A30 floating (or use external pullup) during reset.
* This table does not list "L21 Only" COM configurations.

Alright Board seems to be fully functional. Some updates though.  Driving Vref through the DAC has some issues as it acts as though it is rather low impedance.  after ~1.2V it seems to sink too much current and the output becomes unstable.  Replaced the 0 ohm jumper with a 100K and things are stable across the full scale now.  Still run into thermal issues but that is to be expected without any heat sinks.
micro stepping works and is awesome.  
still having issues with the boards not being detected properly.  I'm guessing this is due to some driver conflict that I've yet to investigate.  Will check on linux (no driver needed) and windows desktop which I don't think has a driver installed yet, and see where that leads.  On windows desktop using generic microsoft drivers it seems to work a bit more frequently but still fails quite often.  Device descriptor request fail(much more common) or set address fail.  Will have to look into this.  Tried so many different bootloaders to no avail.  Will try on linux and see what's up there.  Not sure if this is a noise issue b/c poor routing or if there are some edits I can make in a bootloader that would help alleviate this. migt try to buy a more commercial D21E18 board and see if it has the same issues but they seem to mostly use the D21G for the extra pins.

Next steps...
figure out what parameters I want to control and how to do so.
I'm thinking...
Dip switch array  for setting microstepping (11 options so 2^4 is sufficient but not all used) maybe a button to force an update and read from the pins so it's not acting wonky while you configure
pot run out for setting current with a little OLED showing the calculated value
switch for direction I guess
Knob for step speed.  Maybe a little funky here.  Definitely want to be able to skip steps by going too fast for demonstration but not sure if scale of pot would be a function of step resolution.
proper heat sinking so that it can handle full scale current.  Not super sure if stick on will work well or if they'll fall off over time.
  quick test of stick on heat sink from 302 kit is not sufficient.  Probably a bottom ground plane with some vias and a chunkier sink with more mass?  Might need actual termal paste and decent sink if we really want high speed full current full stepping.
Maybe 3 boards? tiny servo, mid range nema 17/23, chonky boy? really big one seems overkill so will likely just do 2 for now.
proper firmware.  Might do dual versions, typical arduino and low level version not that it matters here in the slightest but it is good practice.

Actually let's kill 3 step options, the 2 that require a 330K to gnd, and non-circular half step.  That will give 2^3 options and then we won't even need the update button.  Might keep that anyway though. idk. maybe not.

-----------------------------
06/25/21
Ok a bit more info on the usb issues.  I think it's pointing to a bootloader problem.  The samd21E17A doesn't seem to have any issues on the same board.  I pulled off the LED and the DRV to make sure it wasn't related to pin state/loading on startup but that didn't help.  Using the arduino burn bootloader rather than the atmel studio option seems to increase the likelyhood that it starts up properly but it isn't 100%.  Maybe closer to 80/85% which isn't too bad I guess.  This is just with single click resets.  Double click resets (to get into the bootloader) are quite unreliable.    using arduino defaults but with usb calibrated oscillator and CDC_HID

I'm wondering if this has something to do with the speed.  Maybe it's only usb 2.0 speed but my laptop is trying to run it at 3.0 sometimes?

Some google pointed to disabling windows ability to suspend USB devices but none of that worked.

E17A seems to work great.  Put E18 onto bare board and still had the same issues.

-----------------------------
06/28/21
Alright problem with usb detection finally solved.  I had Vddcore tied to 3.3V thinking it was an input.  Actually is output from internal regulator and probably destabilized the core voltage on reset which failed the usb handshake. Why did it work at all? why did it sometimes work perfectly? Why did the E17 not care? who knows. unsatisfying but successful fix just have to do a quick board touch up.  Should have a 1 uF cap on Vddcore but technically worked with the leg lifted.

Switched to the Gemma_m0 bootloader.  Not sure if this helped in any way but it has UF2 and is written by adafruit rather than some random dude that stopped work a few years ago so I trust it a little more.  link the bin somewhere so it is easy for students to find.
MattairTech ones are probably still a good starting point as it has so many more board options.

[Adafruit bootloaders](https://github.com/adafruit/uf2-samdx1/releases)
![Schematic](images/D21StepperSch.png )

![Board File](images/D21StepperBrd.png )

![Assembled Image](images/D21StepBoardReal.png )

![Spinning Stepper](images/spinningStepper.mp4 )

-----------------------------
06/29/21

Next goals...
- reroute base board that will serve as a paste-in starting point.  samd headers power usb connector and supporting components.  Nothing else.  Routing borrowed from Jake as that is pretty elegant.  2 layer board with bottom ground. vias to drop down to it which makes good ground routing pretty easy.  Probably polygon pour the top with a ground as well with 1/32" isolation and then manual traces to connect islands if needed.
- update drv board with new routing
  - add 3 pin dip switch for setting microstepping (maybe out to headers?)
  - 2 potentiometers, step delay and current
  - switch for direction? do we really care about this?  I don't think so...
  - use pin change interrupt so we don't read pins excessively
  - Hmm if this is going to get attached to a linkage demo then maybe there should be an easy switch to stop the stepper? Can't hurt but also maybe not necessary 


-----------------------------
06/30/21

Routed the origin for future boards and took a stab at the stepper board with a 3 switch array and 2 pots.  Uses top and bottom ground pours with 32 mil isolation but will likely still be a pretty lengthy mill time. couple vias to conenct the planes but not too many. Pots are through-hole.  power and signal need to be soldered on top.  GND soldered on bottom. Ditched power kill switch as this is very likely not the last version anyway.  Should also probably add a header for 5V and ground? I guess could just be miniusb wallwart supply so unnecessary.  

Got some code to write but it should be straightforward.  Maybe setting up the ISRs will be tricky depending on how well they're implemented and whether I need to touch some registers.

Also figured out in Eagle how to do ground pours a little better.  In the DRC menu there is a "Supply" tab that sets thermal breaks.  upping this to 16 ensures that the 1/64" can fit in there.  Honestly won't really affect performance much and will make things slower to mill but it's nice to not place components on bare copper.

![Interactive Board Schematic](images/InteractSchImage.png )

![Interactive Board](images/InteractBrdImage.png )

![With ground planes](images/InteractBrdPlanesImage.png )

Dip switches set the stepping mode while the potentiometers handle the stepping speed and the max current to the motor. GND plane isolation updates made to reduce milling time a bit.  Couple vias to solder between planes 

--------------
07/14/21

What to do about oxidation?  In theory these should last a long time and will probably be touched frequently.  There are a number of chemical processes that you can put on top.  Might look into liquid tin.  No conformal coating as I want to probe it if needed.  Might try a diy HASL.  Would make a solder bath, dip the board, keep it warm and hit it with a stream of compressed air.  will likely throw solder everywhere so maybe this is a good use for that sanding booth that sits unused in EDS. Could be fun. Great source for lead free hasl info [here](https://www.circuitinsight.com/pdf/hot_air_level_soldering.pdf)

Going to try without a hot air knife.  Regular compressed air hookup is far from ideal as the cool air will want to solidify the solder but maybe at high enough pressures it will perform.  If it doesn't then how do we heat the air?  First a mixing nozzle will help bring it up to temp a bit.  If that isn't sufficient then some kind of heated aluminium cylinder with a slit in it for the air to leave will get higher temp and more planar flow which can't hurt.  Basically would be a fixed instrument on the top with maybe some nichrome wire for heating and an internal temp probe to control the temp if needed? Very far from ideal but might be functional enough. Sounds fun.  Looked at vortex tubes but I don't think it'd be enough airflow.  Could also try modding one of the hot air stations with a smaller nozzle and a better fan?
